/**************************************************************************************************
  Filename:       keyfobdemo.c
  Revised:        $Date: 2013-08-15 15:28:40 -0700 (Thu, 15 Aug 2013) $
  Revision:       $Revision: 34986 $

  Description:    Key Fob Demo Application.

  Copyright 2009 - 2013 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED 揂S IS?WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <string.h>
#include "bcomdef.h"
#include "OSAL.h"
#include "osal_snv.h"
#include "OSAL_PwrMgr.h"

#include "OnBoard.h"
#include "hal_adc.h"
#include "hal_led.h"
#include "hal_key.h"

#include "buzzer.h"
/*
#if defined ( ACC_BMA250 )
#  include "bma250.h"
#elif defined ( ACC_CMA3000 )
#  include "cma3000d.h"
#endif
*/
#include "gatt.h"

#include "hci.h"

#include "gapgattserver.h"
#include "gattservapp.h"
#include "gatt_profile_uuid.h"

#if defined ( PLUS_BROADCASTER )
asdf
#include "peripheralBroadcaster.h"
#else
#include "peripheral.h"
#endif

#include "gapbondmgr.h"
#include "gliese.h"
#include "devinfoservice.h"
#include "proxreporter.h"
#include "battservice.h"
#include "accelerometer.h"
#include "simplekeys.h"
#include "ccservice.h"
#include "keyfobdemo.h"

#if defined(FEATURE_OAD)
#include "oad.h"
#include "oad_target.h"
#endif
#include "knocking.h"
#if defined(DEIMOS_TEMPERATURE)
#include "i2c.h"
#include "tmp112service.h"
#endif

/*********************************************************************
 * MACROS
 */
/* Ative delay: 125 cycles ~1 msec */
#define KFD_HAL_DELAY(n) st( { volatile uint32 i; for (i=0; i<(n); i++) { }; } )

/*********************************************************************
 * CONSTANTS
 */

// Delay between power-up and starting advertising (in ms)
#define STARTDELAY                            500

// Number of beeps before buzzer stops by itself
#define BUZZER_MAX_BEEPS                      15

// Buzzer beep tone frequency for "High Alert" (in Hz)
#define BUZZER_ALERT_HIGH_FREQ                1024   /*4096*/

// Buzzer beep tone frequency for "Low Alert" (in Hz)
#define BUZZER_ALERT_LOW_FREQ                 250

// How often to check battery voltage (in ms)
#define BATTERY_CHECK_PERIOD                  15000

// How often (in ms) to read the accelerometer
//#define ACCEL_READ_PERIOD                     50

// Minimum change in accelerometer before sending a notification
//#define ACCEL_CHANGE_THRESHOLD                5

//GAP Peripheral Role desired connection parameters

// Use limited discoverable mode to advertise for 30.72s, and then stop, or
// use general discoverable mode to advertise indefinitely
//#define DEFAULT_DISCOVERABLE_MODE             GAP_ADTYPE_FLAGS_LIMITED
#define DEFAULT_DISCOVERABLE_MODE             GAP_ADTYPE_FLAGS_GENERAL

#if 1
/* Apple Connnection Params Limits
 1. Interval Max * (Slave Latency + 1) ≤ 2 seconds；
 2. Interval Min ≥ 20 ms；
 3. Interval Min + 20 ms ≤ Interval Max；
 4. Slave Latency ≤ 4；
 5. connSupervisionTimeout ≤ 6 seconds
 6. Interval Max * (Slave Latency + 1) * 3 < connSupervisionTimeout
 */
// Minimum connection interval (units of 1.25ms, 80=100ms) if automatic parameter update request is enabled
// 1500 = 1875ms
// 96 = 120ms
// 176 = 220ms
// 189 = 236.25ms
// 302 = 377.5ms
// 303 = 378.75ms
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     303

// Maximum connection interval (units of 1.25ms, 800=1000ms) if automatic parameter update request is enabled
// 1522 = 1902.5ms
// 190 = 237.5ms
// 192 = 240ms --> On iOS7.0.4, this is the max value!!!
// 200 = 250ms
// 210 = 262.5ms
// 318 = 397.5ms
// 319 = 398.75ms
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     319

// Slave latency to use if automatic parameter update request is enabled
#define DEFAULT_DESIRED_SLAVE_LATENCY         4

// Supervision timeout value (units of 10ms, 1000=10s) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_CONN_TIMEOUT          600
#else
// Minimum connection interval (units of 1.25ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     80

// Maximum connection interval (units of 1.25ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     800

// Slave latency to use if automatic parameter update request is enabled
#define DEFAULT_DESIRED_SLAVE_LATENCY         0

// Supervision timeout value (units of 10ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_CONN_TIMEOUT          1000
#endif
// Whether to enable automatic parameter update request when a connection is formed
#define DEFAULT_ENABLE_UPDATE_REQUEST         FALSE

// Connection Pause Peripheral time value (in seconds)
#define DEFAULT_CONN_PAUSE_PERIPHERAL         2

//#define iBeacon

//Minimum advertising interval, when in General discoverable mode (n * 0.625 mSec)
// 1600 * 0.625 = 1000ms
// 3200 * 0.625 = 2000ms
// 8000 * 0.625 = 5000ms
// 16000 * 0.625 = 10000ms
// 12191 * 0.625 = 7619.375ms

#if 1
//#define DEFAULT_DISC_ADV_INT_MIN         3200 //1600
//#define DEFAULT_DISC_ADV_INT_MID         3200  //8000
//#define DEFAULT_DISC_ADV_INT_MAX         3200 //16000
//static uint16 gAdvIntervalMin = 3200;
//static uint16 gAdvIntervalMax = 3200;
static uint16 gAdvIntervalMin = 1600;
static uint16 gAdvIntervalMax = 4800;
static uint8 gAdvState = 0;
#else
//#define DEFAULT_DISC_ADV_INT_MIN         1600
//#define DEFAULT_DISC_ADV_INT_MID         8000
//#define DEFAULT_DISC_ADV_INT_MAX         12191
static uint16 gAdvIntervalMin = 1600;
static uint16 gAdvIntervalMax = 12191;
#endif

//#define DEFAULT_LIM_DISC_ADV_INT_MIN         DEFAULT_DISC_ADV_INT_MIN
//#define DEFAULT_LIM_DISC_ADV_INT_MID         DEFAULT_DISC_ADV_INT_MID
//#define DEFAULT_LIM_DISC_ADV_INT_MAX         DEFAULT_DISC_ADV_INT_MAX

//#define DEFAULT_GEN_DISC_ADV_INT_MIN         DEFAULT_DISC_ADV_INT_MIN
//#define DEFAULT_GEN_DISC_ADV_INT_MID         DEFAULT_DISC_ADV_INT_MID
//#define DEFAULT_GEN_DISC_ADV_INT_MAX         DEFAULT_DISC_ADV_INT_MAX

// keyfobProximityState values
#define KEYFOB_PROXSTATE_INITIALIZED          0   // Advertising after initialization or due to terminated link
#define KEYFOB_PROXSTATE_CONNECTED_IN_RANGE   1   // Connected and "within range" of the master, as defined by
                                                  // proximity profile
#define KEYFOB_PROXSTATE_PATH_LOSS            2   // Connected and "out of range" of the master, as defined by
                                                  // proximity profile
#define KEYFOB_PROXSTATE_LINK_LOSS            3   // Disconnected as a result of a supervision timeout

// buzzer_state values
#define BUZZER_OFF                            0
#define BUZZER_ON                             1

// keyfobAlertState values
#define ALERT_STATE_OFF                       0
#define ALERT_STATE_LOW                       1
#define ALERT_STATE_HIGH                      2

// Company Identifier: Texas Instruments Inc. (13)
#define TI_COMPANY_ID                         0x000D

#define INVALID_CONNHANDLE                    0xFFFF

#if defined ( PLUS_BROADCASTER )
#define ADV_IN_CONN_WAIT                    500 // delay 500 ms
#endif
// Default passcode
//#define DEFAULT_PHOBOS_PASSCODE               1976
static uint32 gDefaultPasscode = 1976;
static uint32 gTemperatureCheckInterval; //ms

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
uint8 keyfobapp_TaskID;   // Task ID for internal task/event processing

static gaprole_States_t gapProfileState = GAPROLE_INIT;

// Proximity State Variables
static uint8 keyfobProxLLAlertLevel = PP_ALERT_LEVEL_NO;     // Link Loss Alert
static uint8 keyfobProxIMAlertLevel = PP_ALERT_LEVEL_NO;     // Link Loss Alert
static int8  keyfobProxTxPwrLevel = 0;  // Tx Power Level (0dBm default)

// keyfobProximityState is the current state of the device
static uint8 keyfobProximityState;

static uint8 keyfobAlertState;
//static uint8 keyfobAdvState = 0;

/*
// GAP - SCAN RSP data (max size = 31 bytes)
static uint8 deviceName[] =
{
  // complete name
  0x07,   // length of first data structure (11 bytes excluding length byte)
  GAP_ADTYPE_LOCAL_NAME_COMPLETE,   // AD Type = Complete local name
  0x50,   // 'P'
  0x68,   // 'h'
  0x6f,   // 'o'
  0x62,   // 'b'
  0x6f,   // 'o'
  0x73,   // 's'
};
*/

// GAP Profile - Name attribute for SCAN RSP data
static uint8 scanResponseData[] =
{
    0x07,   // length of this data
    GAP_ADTYPE_LOCAL_NAME_COMPLETE,
    0x50,   // 'P'
    0x68,   // 'h'
    0x6f,   // 'o'
    0x62,   // 'b'
    0x6f,   // 'o'
    0x73,   // 's'
#if 0
    // connection interval range
    0x05,   // length of this data
    GAP_ADTYPE_SLAVE_CONN_INTERVAL_RANGE,
    LO_UINT16( DEFAULT_DESIRED_MIN_CONN_INTERVAL ),   // 100ms
    HI_UINT16( DEFAULT_DESIRED_MIN_CONN_INTERVAL ),
    LO_UINT16( DEFAULT_DESIRED_MAX_CONN_INTERVAL ),   // 1s
    HI_UINT16( DEFAULT_DESIRED_MAX_CONN_INTERVAL ),
#endif
    0x02,   // length of this data
    GAP_ADTYPE_POWER_LEVEL, //TX Power Level
    0       // 0dBm
};

// GAP - Advertisement data (max size = 31 bytes, though this is
// best kept short to conserve power while advertisting)

#if 1
/* For iBeacon adver data:
# Actual Advertising Data Starts Here
02 01 1a
1a ff 4c 00 02 15 # Apple's static prefix to the advertising data -- this is always the same
e2 c5 6d b5 df fb 48 d2 b0 60 d0 f5 a7 10 96 e0 # iBeacon profileUUID
00 00 # major (LSB first)
00 00 # minor (LSB first)
c5 # The 2's complement of the calibrated Tx Power
 */
static uint8 advertDataBeacon[] =
{
    0x02,   // length of first data structure (2 bytes excluding length byte)
    GAP_ADTYPE_FLAGS,   // AD Type = Flags
    0x1a,

    // service UUID, to notify central devices what services are included
    // in this peripheral
    0x1B,   // length of second data structure (7 bytes excluding length byte)
    0xff, 0x4c, 0x00, 0x02, 0x15,
    //0xe2, 0xc5, 0x6d, 0xb5, 0xdf, 0xfb, 0x48, 0xd2, 0xb0, 0x60, 0xd0, 0xf5, 0xa7, 0x10, 0x96, 0xe0,
    0x91, 0x5F, 0xF9, 0xB4, 0xAB, 0x65, 0x47, 0xB7, 0xBC, 0x1B, 0xA4, 0xA3, 0x9B, 0xA2, 0xBF, 0xA6,
    0x00, 0x00,
    0x00, 0x00,
    0xc5,
    0xBB,
};

static uint8 advertDataSmall[] =
{
    0x02,   // length of first data structure (2 bytes excluding length byte)
    GAP_ADTYPE_FLAGS,   // AD Type = Flags
    DEFAULT_DISCOVERABLE_MODE | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,

    // service UUID, to notify central devices what services are included
    // in this peripheral
#if defined(HAL_IMAGE_A)
    0x0B,   // length of second data structure
    GAP_ADTYPE_16BIT_MORE,   // list of 16-bit UUID's available, but not complete list
    //LO_UINT16( DEVINFO_SERV_UUID ),        // Device Information
    //HI_UINT16( DEVINFO_SERV_UUID ),
    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, // GAPROLE_BD_ADDR
    0x00, 0xAA, // Battery value
    0xFF, 0xEE, // Temperature
#else
    0x0B,
    GAP_ADTYPE_16BIT_MORE,
    //LO_UINT16( IMMEDIATE_ALERT_SERV_UUID ),  // Immediate Alert Service (Proximity / Find Me Profile)
    //HI_UINT16( IMMEDIATE_ALERT_SERV_UUID ),
    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, // GAPROLE_BD_ADDR
    0x00, 0xAA, // Battery value
    0xFF, 0xEE, // Temperature
#endif
};
#else
static uint8 advertData[] =
{
    0x02,   // length of first data structure (2 bytes excluding length byte)
    GAP_ADTYPE_FLAGS,   // AD Type = Flags
    DEFAULT_DISCOVERABLE_MODE | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,

    // service UUID, to notify central devices what services are included
    // in this peripheral
#if defined(HAL_IMAGE_B)
    0x03,   // length of second data structure (7 bytes excluding length byte)
    GAP_ADTYPE_16BIT_MORE,   // list of 16-bit UUID's available, but not complete list
    LO_UINT16( LINK_LOSS_SERV_UUID ),        // Link Loss Service (Proximity Profile)
    HI_UINT16( LINK_LOSS_SERV_UUID ),
#else
    //  0x03,
    //  GAP_ADTYPE_16BIT_MORE,
    //  LO_UINT16( IMMEDIATE_ALERT_SERV_UUID ),  // Immediate Alert Service (Proximity / Find Me Profile)
    //  HI_UINT16( IMMEDIATE_ALERT_SERV_UUID ),
    //  LO_UINT16( TX_PWR_LEVEL_SERV_UUID ),     // Tx Power Level Service (Proximity Profile)
    //  HI_UINT16( TX_PWR_LEVEL_SERV_UUID )
#endif
};
#endif

#if defined(HAL_IMAGE_A)
// GAP GATT Attributes
const static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "HUOWEIYIA";
#elif defined(HAL_IMAGE_B)
const static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "HUOWEIYIB";
#else
const static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "HUOWEIYI";
#endif

// Buzzer state
static uint8 buzzer_state = BUZZER_OFF;
static uint8 buzzer_beep_count = 0;

#if defined(KNOCK_WAKE_UP)
static uint16 gKnockingCount = 0;
static uint8 gHadWakeUp = 0;
#endif

static uint8 gBatteryValue = 100;

// Accelerometer Profile Parameters
//static uint8 accelEnabler = FALSE;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void keyfobapp_ProcessOSALMsg( osal_event_hdr_t *pMsg );
static void keyfobapp_PerformAlert( void );
static void keyfobapp_PerformAlertMusic( );
static void keyfobapp_StopAlert( void );
static void keyfobapp_HandleKeys( uint8 shift, uint8 keys );
static void peripheralStateNotificationCB( gaprole_States_t newState );
static void proximityAttrCB( uint8 attrParamID );
static void phobosRateBattCB(uint8 event);
static void ccServiceChangeCB( uint8 paramID );
static uint8 gPairStatus = 0; /*用来管理当前的状态，如果密码不正确，立即取消连接，0表示未配对，1表示已配对*/
static void glieseServiceChangeCB( uint8 paramID );
static void ProcessPasscodeCB(uint8 *deviceAddr, uint16 connectionHandle, uint8 uiInputs, uint8 uiOutputs );
static void ProcessPairStateCB( uint16 connHandle, uint8 state, uint8 status );
//static void accelEnablerChangeCB( void );
//static void accelRead( void );
#if defined(DEIMOS_TEMPERATURE)
static void readTMP112Data( void );
static void tmp112ServiceChangeCB( uint8 paramID );
#endif

/*********************************************************************
 * PROFILE CALLBACKS
 */

// GAP Role Callbacks
static gapRolesCBs_t keyFob_PeripheralCBs =
{
    peripheralStateNotificationCB,  // Profile State Change Callbacks
    NULL                // When a valid RSSI is read from controller
};


// GAP Bond Manager Callbacks
static gapBondCBs_t keyFob_BondMgrCBs =
{
    ProcessPasscodeCB,                     // Passcode callback
    ProcessPairStateCB                     // Pairing / Bonding state Callback
};

// Proximity Peripheral Profile Callbacks
static proxReporterCBs_t keyFob_ProximityCBs =
{
    proximityAttrCB,              // Whenever the Link Loss Alert attribute changes
};

static ccCBs_t keyfob_ccCBs =
{
    ccServiceChangeCB,               // Charactersitic value change callback
};

static glieseProfileCBs_t keyfob_GlieseCBs =
{
    glieseServiceChangeCB,               // Charactersitic value change callback
};
#if defined(DEIMOS_TEMPERATURE)
static tmp112ProfileCBs_t keyfob_Tmp112CBs =
{
    tmp112ServiceChangeCB,               // Charactersitic value change callback
};
#endif

// Accelerometer Profile Callbacks
/*
static accelCBs_t keyFob_AccelCBs =
{
  accelEnablerChangeCB,          // Called when Enabler attribute changes
};
*/

//+add by hzb
#if 0
static bool temperatureMeasCharConfig = false;
static bool temperatureIMeasCharConfig = false;
static bool temperatureIntervalConfig = false;
static bool thMeasTimerRunning = FALSE;

static void thermometerCB(uint8 event)
{

    switch (event)
    {
#if 1
    case THERMOMETER_TEMP_IND_ENABLED:
        //osal_set_event( keyfobapp_TaskID, TH_CCC_UPDATE_EVT );
        break;

    case  THERMOMETER_TEMP_IND_DISABLED:
        temperatureMeasCharConfig = false;
        //osal_stop_timerEx( keyfobapp_TaskID, TH_PERIODIC_MEAS_EVT );
        thMeasTimerRunning = FALSE;
        break;

    case THERMOMETER_IMEAS_NOTI_ENABLED:
        temperatureIMeasCharConfig = true;
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            //osal_start_timerEx( keyfobapp_TaskID, TH_PERIODIC_IMEAS_EVT, 1000 );
        }
        break;

    case  THERMOMETER_IMEAS_NOTI_DISABLED:
        temperatureIMeasCharConfig = false;
        //osal_stop_timerEx( keyfobapp_TaskID, TH_PERIODIC_IMEAS_EVT );
        break;

    case THERMOMETER_INTERVAL_IND_ENABLED:
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            temperatureIntervalConfig = true;
        }
        break;

    case  THERMOMETER_INTERVAL_IND_DISABLED:
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            temperatureIntervalConfig = false;
        }
        break;
#endif

    default:
        break;
    }
}
#endif
//-add by hzb

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      KeyFobApp_Init
 *
 * @brief   Initialization function for the Key Fob App Task.
 *          This is called during initialization and should contain
 *          any application specific initialization (ie. hardware
 *          initialization/setup, table initialization, power up
 *          notificaiton ... ).
 *
 * @param   task_id - the ID assigned by OSAL.  This ID should be
 *                    used to send messages and set timers.
 *
 * @return  none
 */
void KeyFobApp_Init( uint8 task_id )
{
    keyfobapp_TaskID = task_id;

    uint8 ret = 1;
    // Setup the GAP
    VOID GAP_SetParamValue( TGAP_CONN_PAUSE_PERIPHERAL, DEFAULT_CONN_PAUSE_PERIPHERAL );
#if defined(KNOCK_WAKE_UP)
    uint8 ifEverWakeup = 0;
    ret = osal_snv_read(BLE_NVID_KNOCK_WAKE_UP, 1, &ifEverWakeup);
    if (ret == SUCCESS)
    {
        gHadWakeUp = ifEverWakeup;
    }
    else
    {
        gHadWakeUp = 0;
    }
#endif
#if defined(DEIMOS_TEMPERATURE)
    uint8 tempInterval[2] = {0};
    ret = osal_snv_read(BLE_NVID_TEMPERATURE_CHECK_INTERVAL, TMP112PROFILE_CHAR1_LEN-4, tempInterval);
    if (ret == SUCCESS)
    {
        uint16 temp = tempInterval[0]<<8 | tempInterval[1];
        if (temp < 4)
        {
            temp = 4;
        }
        gTemperatureCheckInterval = temp * 1000;
    }
    else
    {
        gTemperatureCheckInterval = 4000;
    }
#endif
#if defined(READ_INTERNAL_TEMPERATURE)
    int8 chazhi = 0;
    ret = osal_snv_read(BLE_NVID_CC2541_INTERNAL_TEMPERATURE_CALIBRATION, 1, &chazhi);
    if (ret == SUCCESS)
    {
        uint8 buffers[GLIESEPROFILE_CHAR7_LEN];
        buffers[8] = chazhi;
        GlieseProfile_SetParameter(GLIESEPROFILE_CHAR7, GLIESEPROFILE_CHAR7_LEN, buffers);
    }
    else
    {
        // Do nothing.
    }
#endif
#if defined(iBeacon)
    uint8 beacon[GLIESEPROFILE_CHAR2_LEN] = {0};
    ret = osal_snv_read(BLE_NVID_iBeacon_SET, GLIESEPROFILE_CHAR2_LEN, beacon);
    if (ret == SUCCESS)// set iBeacon UUID
    {
        osal_memcpy(advertDataBeacon + 9, beacon, 16);
    }
#endif
    uint8 beacon1[GLIESEPROFILE_CHAR1_LEN-4] = {0};
    ret = osal_snv_read(BLE_NVID_iBeacon_SET1, GLIESEPROFILE_CHAR1_LEN-4, beacon1);
    if (ret == SUCCESS)
    {
        // set major
        advertDataBeacon[25] = beacon1[0];
        advertDataBeacon[26] = beacon1[1];

        // set minor
        advertDataBeacon[27] = beacon1[2];
        advertDataBeacon[28] = beacon1[3];
        GlieseProfile_SetParameter(GLIESEPROFILE_CHAR1, GLIESEPROFILE_CHAR1_LEN-4, beacon1);
        // set The 2's complement of the calibrated Tx Power
        advertDataBeacon[29] = beacon1[4];
#if 0
        //Tx Power
        uint8 txPower = HCI_EXT_TX_POWER_0_DBM;
        switch (beacon1[5])
        {
        case 0:
            txPower = HCI_EXT_TX_POWER_MINUS_23_DBM;
            break;

        case 1:
            txPower = HCI_EXT_TX_POWER_MINUS_6_DBM;
            break;

        case 2:
            txPower = HCI_EXT_TX_POWER_0_DBM;
            break;

        case 3:
            txPower = HCI_EXT_TX_POWER_4_DBM;
            break;
        }
        HCI_EXT_SetTxPowerCmd( txPower );
#endif
        //Advertising Freq (1-100) Unit: 100ms
        if (beacon1[6] != 0)
        {
            gAdvIntervalMin = (uint16)((float)beacon1[6] * (float)100 / (float)0.625);
            if (gAdvIntervalMin < 1600)
            {
                gAdvIntervalMin = 1600;
            }
            gAdvIntervalMax = gAdvIntervalMin;
        }
    }

    // Set fast advertising interval for user-initiated connections
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, gAdvIntervalMin );
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, gAdvIntervalMin );
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 15000 );

    // Setup the GAP Peripheral Role Profile
    {
        // For the CC2540DK-MINI keyfob, device doesn't start advertising until button is pressed
        uint8 initial_advertising_enable = FALSE;

        // By setting this to zero, the device will go into the waiting state after
        // being discoverable for 30.72 second, and will not being advertising again
        // until the enabler is set back to TRUE
        uint16 gapRole_AdvertOffTime = 0;

        uint8 enable_update_request = DEFAULT_ENABLE_UPDATE_REQUEST;
        uint16 desired_min_interval = DEFAULT_DESIRED_MIN_CONN_INTERVAL;
        uint16 desired_max_interval = DEFAULT_DESIRED_MAX_CONN_INTERVAL;
        uint16 desired_slave_latency = DEFAULT_DESIRED_SLAVE_LATENCY;
        uint16 desired_conn_timeout = DEFAULT_DESIRED_CONN_TIMEOUT;

        // Set the GAP Role Parameters
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &initial_advertising_enable );
        GAPRole_SetParameter( GAPROLE_ADVERT_OFF_TIME, sizeof( uint16 ), &gapRole_AdvertOffTime );

        GAPRole_SetParameter( GAPROLE_SCAN_RSP_DATA, sizeof ( scanResponseData ), scanResponseData );
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataSmall ), advertDataSmall );

        GAPRole_SetParameter( GAPROLE_PARAM_UPDATE_ENABLE, sizeof( uint8 ), &enable_update_request );
        GAPRole_SetParameter( GAPROLE_MIN_CONN_INTERVAL, sizeof( uint16 ), &desired_min_interval );
        GAPRole_SetParameter( GAPROLE_MAX_CONN_INTERVAL, sizeof( uint16 ), &desired_max_interval );
        GAPRole_SetParameter( GAPROLE_SLAVE_LATENCY, sizeof( uint16 ), &desired_slave_latency );
        GAPRole_SetParameter( GAPROLE_TIMEOUT_MULTIPLIER, sizeof( uint16 ), &desired_conn_timeout );
    }

    // Set the GAP Attributes
    uint8 deviceNamePhobos[GAP_DEVICE_NAME_LEN] = {0};
    ret = osal_snv_read(BLE_NVID_PHOBOS_DEVICE_NAME, GAP_DEVICE_NAME_LEN, deviceNamePhobos);
    if (ret == SUCCESS)
    {
        GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, deviceNamePhobos );
    }
    else
    {
        GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, (void *)attDeviceName );
    }

    // Setup the GAP Bond Manager
    {
        uint32 passkey = gDefaultPasscode;
        uint8 pairMode = GAPBOND_PAIRING_MODE_WAIT_FOR_REQ;
        uint8 mitm = TRUE;
        uint8 ioCap = GAPBOND_IO_CAP_DISPLAY_ONLY;
        uint8 bonding = TRUE;

        uint8 bondSet[8];
        ret = osal_snv_read(BLE_NVID_BOND_SETTING, 8, bondSet);
        if (ret == SUCCESS)
        {
            passkey = ((uint32)bondSet[0]<<24)|
                ((uint32)bondSet[1]<<16)|
                ((uint32)bondSet[2]<<8)|
                ((uint32)bondSet[3]);
            pairMode = bondSet[4];
            mitm = bondSet[5];
            ioCap = bondSet[6];
            bonding = bondSet[7];
            gDefaultPasscode = passkey;
            GlieseProfile_SetParameter(GLIESEPROFILE_CHAR10, GLIESEPROFILE_CHAR10_LEN-6, bondSet);
        }
        else
        {
            bondSet[0] = (passkey>>24) & 0xFF;
            bondSet[1] = (passkey>>16) & 0xFF;
            bondSet[2] = (passkey>>8) & 0xFF;
            bondSet[3] = passkey& 0xFF;
            bondSet[4] = pairMode;
            bondSet[5] = mitm;
            bondSet[6] = ioCap;
            bondSet[7] = bonding;
            GlieseProfile_SetParameter(GLIESEPROFILE_CHAR10, GLIESEPROFILE_CHAR10_LEN-6, bondSet);
        }

        GAPBondMgr_SetParameter( GAPBOND_DEFAULT_PASSCODE, sizeof ( uint32 ), &passkey );
        GAPBondMgr_SetParameter( GAPBOND_PAIRING_MODE, sizeof ( uint8 ), &pairMode );
        GAPBondMgr_SetParameter( GAPBOND_MITM_PROTECTION, sizeof ( uint8 ), &mitm );
        GAPBondMgr_SetParameter( GAPBOND_IO_CAPABILITIES, sizeof ( uint8 ), &ioCap );
        GAPBondMgr_SetParameter( GAPBOND_BONDING_ENABLED, sizeof ( uint8 ), &bonding );
    }
    // Initialize GATT attributes
    GGS_AddService( GATT_ALL_SERVICES );         // GAP
    GATTServApp_AddService( GATT_ALL_SERVICES ); // GATT attributes
#if defined(FEATURE_OAD)
    VOID OADTarget_AddService(); //OAD Profile
#endif
    DevInfo_AddService();   // Device Information Service
#if defined(DEIMOS_TEMPERATURE)
    Tmp112Profile_AddService( GATT_ALL_SERVICES );
#endif
    ProxReporter_AddService( GATT_ALL_SERVICES );  // Proximity Reporter Profile
    Batt_AddService( );     // Battery Service
    CcService_AddService( GATT_ALL_SERVICES );     // Connection Control Service
    GlieseProfile_AddService( GATT_ALL_SERVICES );  // Gliese Profile for Phobos
    //Accel_AddService( GATT_ALL_SERVICES );      // Accelerometer Profile
    //SK_AddService( GATT_ALL_SERVICES );         // Simple Keys Profile

    //+add by hzb
#if 0
    // Setup the Thermometer Characteristic Values
    {
        uint8 thermometerSite = THERMOMETER_TYPE_MOUTH;
        Thermometer_SetParameter( THERMOMETER_TYPE, sizeof ( uint8 ), &thermometerSite );

        thermometerIRange_t thermometerIRange = {4, 60};
        Thermometer_SetParameter( THERMOMETER_IRANGE, sizeof ( uint16 ), &thermometerIRange );
    }

    Thermometer_AddService( GATT_ALL_SERVICES );   //add by hzb
    DevInfo_AddService( );
    // Register for Thermometer service callback
    Thermometer_Register ( thermometerCB );
#endif
    //-add by hzb

    keyfobProximityState = KEYFOB_PROXSTATE_INITIALIZED;

    // Initialize Tx Power Level characteristic in Proximity Reporter
    {
        int8 initialTxPowerLevel = 0;

        ProxReporter_SetParameter( PP_TX_POWER_LEVEL, sizeof ( int8 ), &initialTxPowerLevel );
    }

    keyfobAlertState = ALERT_STATE_OFF;

    // make sure buzzer is off
    buzzerStop();

    // makes sure LEDs are off
    HalLedSet( (HAL_LED_2), HAL_LED_MODE_OFF );

    // For keyfob board set GPIO pins into a power-optimized state
    // Note that there is still some leakage current from the buzzer,
    // accelerometer, LEDs, and buttons on the PCB.
    P0SEL = 0x00; // Configure Port 0 as GPIO
    P1SEL = 0x00; // Configure Port 1 as GPIO
    P2SEL = 0;    // Configure Port 2 as GPIO

    //P0INP |= 0x40;              // P0_6 as 3-state  //add by hzb
    //P1INP |= 0x20;              // P1_5 as 3-state  //add by hzb

    P0DIR = 0xFF; // Port 0 pins P0.6 as input (temperature) //modify by hzb
    // all others as output
    P1DIR = 0xFF; //P1.5 as input (buzzer and led) other pins as output
    P2DIR = 0x1F; // All port 1 pins (P2.0-P2.4) as output

    //APCFG |= 0x40;             // AIN6 for temperature  //add by hzb
    P0 = 0x0; // All pins on port 0 to low
    P1 = 0x0; // All pins on port 1 to low
    P2 = 0;   // All pins on port 2 to low


    // initialize the ADC for battery reads
    HalAdcInit();

    // Register for all key events - This app will handle all key events
    RegisterForKeys( keyfobapp_TaskID );

    // Register for Battery service callback;
    Batt_Register ( phobosRateBattCB );
    GlieseProfile_RegisterAppCBs(&keyfob_GlieseCBs);
    VOID CcService_RegisterAppCBs( &keyfob_ccCBs );
#if defined ( DC_DC_P0_7 )
    // Enable stack to toggle bypass control on TPS62730 (DC/DC converter)
    HCI_EXT_MapPmIoPortCmd( HCI_EXT_PM_IO_PORT_P0, HCI_EXT_PM_IO_PORT_PIN7 );
#endif // defined ( DC_DC_P0_7 )

#if defined(DEIMOS_TEMPERATURE)
    Tmp112Profile_RegisterAppCBs(&keyfob_Tmp112CBs);
    Tmp112Init();
#endif
    // Setup a delayed profile startup
    osal_start_timerEx( keyfobapp_TaskID, KFD_START_DEVICE_EVT, STARTDELAY );
}

/*********************************************************************
 * @fn      KeyFobApp_ProcessEvent
 *
 * @brief   Key Fob Application Task event processor.  This function
 *          is called to process all events for the task.  Events
 *          include timers, messages and any other user defined events.
 *
 * @param   task_id  - The OSAL assigned task ID.
 * @param   events - events to process.  This is a bit map and can
 *                   contain more than one event.
 *
 * @return  none
 */
uint16 KeyFobApp_ProcessEvent( uint8 task_id, uint16 events )
{
    if ( events & SYS_EVENT_MSG )
    {
        uint8 *pMsg;

        if ( (pMsg = osal_msg_receive( keyfobapp_TaskID )) != NULL )
        {
            keyfobapp_ProcessOSALMsg( (osal_event_hdr_t *)pMsg );

            // Release the OSAL message
            VOID osal_msg_deallocate( pMsg );
        }

        // return unprocessed events
        return (events ^ SYS_EVENT_MSG);
    }

    if ( events & KFD_START_DEVICE_EVT )
    {
        // Start the Device
        VOID GAPRole_StartDevice( &keyFob_PeripheralCBs );

        // Start Bond Manager
        VOID GAPBondMgr_Register( &keyFob_BondMgrCBs );

        // Start the Proximity Profile
        VOID ProxReporter_RegisterAppCBs( &keyFob_ProximityCBs );

        // Set timer for first battery read event
        //osal_start_timerEx( keyfobapp_TaskID, KFD_BATTERY_CHECK_EVT, BATTERY_CHECK_PERIOD );
        gBatteryValue = battMeasure();
        advertDataSmall[14] = gBatteryValue;
        advertDataBeacon[30] = gBatteryValue;
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataSmall ), advertDataSmall );

        // Start the Accelerometer Profile
        //VOID Accel_RegisterAppCBs( &keyFob_AccelCBs );

        //Set the proximity attribute values to default
        ProxReporter_SetParameter( PP_LINK_LOSS_ALERT_LEVEL,  sizeof ( uint8 ), &keyfobProxLLAlertLevel );
        ProxReporter_SetParameter( PP_IM_ALERT_LEVEL,  sizeof ( uint8 ), &keyfobProxIMAlertLevel );
        ProxReporter_SetParameter( PP_TX_POWER_LEVEL,  sizeof ( int8 ), &keyfobProxTxPwrLevel );

        // Set LED1 on to give feedback that the power is on, and a timer to turn off
        //HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_ON );
        uint8 timerReset = 0;
        uint8 ret = osal_snv_read(BLE_NVID_TIMER_RESET, 1, &timerReset);
        if (ret == SUCCESS)
        {
            if (timerReset == 0)
            {
                osal_pwrmgr_device( PWRMGR_ALWAYS_ON ); // To keep the LED on continuously.
                buzzerStart( BUZZER_ALERT_HIGH_FREQ );
                buzzer_state = BUZZER_ON;
            }
            else
            {
                uint8 timerReset1 = 0;
                osal_snv_write(BLE_NVID_TIMER_RESET, 1, &timerReset1);
            }
        }
        else
        {
            osal_pwrmgr_device( PWRMGR_ALWAYS_ON ); // To keep the LED on continuously.
            buzzerStart( BUZZER_ALERT_HIGH_FREQ );
            buzzer_state = BUZZER_ON;
        }
        // only run buzzer for 600ms
        osal_start_timerEx( keyfobapp_TaskID, KFD_POWERON_ADVERT_EVT, 600 );
        return ( events ^ KFD_START_DEVICE_EVT );
    }
    if ( events & KFD_POWERON_ADVERT_EVT)
    {
        {
            uint8 current_adv_enabled_status;
            uint8 new_adv_enabled_status;
            //uint8 SK_Keys = SK_KEY_RIGHT;

            buzzer_state = BUZZER_OFF;
            //Find the current GAP advertisement status
            GAPRole_GetParameter( GAPROLE_ADVERT_ENABLED, &current_adv_enabled_status );

            if( current_adv_enabled_status == FALSE )
            {
                new_adv_enabled_status = TRUE;
                //change the GAP advertisement status to opposite of current status
                GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &new_adv_enabled_status );
                //	SK_SetParameter( SK_KEY_ATTR, sizeof ( uint8 ), &SK_Keys );
            }
            HalLedSet( HAL_LED_2, HAL_LED_MODE_ON );
            HalLedSet( HAL_LED_2, HAL_LED_MODE_FLASH );
            buzzerStop();
        }

#if defined ( POWER_SAVING )
        osal_pwrmgr_device( PWRMGR_BATTERY );
#endif
        //HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_OFF );
        //OnBoard_EnableKeys(true);
#if defined(KNOCK_WAKE_UP)
        if (gHadWakeUp)
        {
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_PHOBOS, 86400000 );
            osal_start_timerEx( keyfobapp_TaskID, KFD_UNEXPECTED_ACTIVATE_CHECK_EVT, 120000 );
            //osal_start_timerEx( keyfobapp_TaskID, KFD_TEST_INTERNAL_TEMPERATURE_EVT, 1000);
        #if defined(DEIMOS_TEMPERATURE)
            osal_start_timerEx( keyfobapp_TaskID, KFD_READ_TEMPERATURE_EVT, 2000);
        #endif
        }
        else
        {
            osal_start_timerEx( keyfobapp_TaskID, KFD_BEEP_AGAIN, 300 );
        }
#else
        osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_PHOBOS, 86400000 );
#endif
        return ( events ^ KFD_POWERON_ADVERT_EVT );
    }
#if defined(DEIMOS_TEMPERATURE)
    if (events & KFD_READ_TEMPERATURE_EVT)
    {
        if (Tem112Status() == TMP112_DATA_READY)
        {
            readTMP112Data();
            TMP112OneShotFinished();
            osal_start_timerEx( keyfobapp_TaskID, KFD_READ_TEMPERATURE_EVT, gTemperatureCheckInterval );
        }
        else if (Tem112Status() == TMP112_OFF)
        {
            #if 0
            P1_1 = 1;
            KFD_HAL_DELAY(1250);
            P1_1 = 0;
            #endif
            TMP112OneShot();
            osal_start_timerEx( keyfobapp_TaskID, KFD_READ_TEMPERATURE_EVT, 50 );
        }
        return (events ^ KFD_READ_TEMPERATURE_EVT);
    }
#endif
    if ( events & KFD_BEEP_AGAIN )
    {
        osal_pwrmgr_device( PWRMGR_ALWAYS_ON );
        buzzerStart( 2700 );
        buzzer_state = BUZZER_ON;
        osal_start_timerEx( keyfobapp_TaskID, KFD_BEEP_AGAIN_STOP, 400 );
        return ( events ^ KFD_BEEP_AGAIN );
    }
    if ( events & KFD_BEEP_AGAIN_STOP)
    {
        buzzer_state = BUZZER_OFF;
        buzzerStop();
        osal_pwrmgr_device( PWRMGR_BATTERY );
        knockingEnableP0_7(TRUE);
        return ( events ^ KFD_BEEP_AGAIN_STOP );
    }
    if ( events & KFD_RESET_PHOBOS )
    {
        if (!(gapProfileState == GAPROLE_CONNECTED))
        {
            uint8 timerReset = 1;
            osal_snv_write(BLE_NVID_TIMER_RESET, 1, &timerReset);
            HAL_SYSTEM_RESET();
        }
        else
        {
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_PHOBOS, 3600000 );
        }
        return (events ^ KFD_RESET_PHOBOS);
    }

    if ( events & KFD_RESET_PHOBOS_NOW )
    {
        HAL_SYSTEM_RESET();
        //return (events ^ KFD_RESET_PHOBOS_NOW);
    }

    if ( events & KFD_ADV_BEACON_NORMAL_EVT )
    {
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            return (events ^ KFD_ADV_BEACON_NORMAL_EVT);
        }
        if (gAdvState == 0)
        {
            GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataSmall ), advertDataSmall );
            gAdvState = 1;
        }
        else if (gAdvState == 1)
        {
            GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
            /*  GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, 4800 ); // every 3 seconds.
              GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, 4800 );
              GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 0 );
              GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
              */
            gAdvState = 0;
        }
        osal_start_timerEx( keyfobapp_TaskID, KFD_ADV_BEACON_NORMAL_EVT, 30000 );
        return (events ^ KFD_ADV_BEACON_NORMAL_EVT);
    }
    /*
    if ( events & KFD_ACCEL_READ_EVT )
    {
      bStatus_t status = Accel_GetParameter( ACCEL_ENABLER, &accelEnabler );

      if (status == SUCCESS)
      {
        if ( accelEnabler )
        {
          // Restart timer
          if ( ACCEL_READ_PERIOD )
          {
            osal_start_timerEx( keyfobapp_TaskID, KFD_ACCEL_READ_EVT, ACCEL_READ_PERIOD );
          }

          // Read accelerometer data
          accelRead();
        }
        else
        {
          // Stop the acceleromter
          osal_stop_timerEx( keyfobapp_TaskID, KFD_ACCEL_READ_EVT);
        }
      }
      else
      {
          //??
      }
      return (events ^ KFD_ACCEL_READ_EVT);
    }
    */

    if ( events & KFD_CONN_PARAM_UPDATE_EVT )
    {
        // Send param update.  If it fails, retry until successful.
        GAPRole_SendUpdateParam( DEFAULT_DESIRED_MIN_CONN_INTERVAL, DEFAULT_DESIRED_MAX_CONN_INTERVAL,
                                 DEFAULT_DESIRED_SLAVE_LATENCY, DEFAULT_DESIRED_CONN_TIMEOUT,
                                 GAPROLE_NO_ACTION);
        return (events ^ KFD_CONN_PARAM_UPDATE_EVT);
    }
    if ( events & KFD_BATTERY_CHECK_EVT )
    {
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            // Restart timer
            if ( BATTERY_CHECK_PERIOD )
            {
                osal_start_timerEx( keyfobapp_TaskID, KFD_BATTERY_CHECK_EVT, BATTERY_CHECK_PERIOD );
            }
        }

        // perform battery level check
        Batt_MeasLevel( );

        return (events ^ KFD_BATTERY_CHECK_EVT);
    }

    if ( events & KFD_TOGGLE_BUZZER_EVT )
    {
        // if this event was triggered while buzzer is on, turn it off, increment beep_count,
        // check whether max has been reached, and if not set the OSAL timer for next event to
        // turn buzzer back on.
        if ( buzzer_state == BUZZER_ON )
        {
            buzzerStop();
            buzzer_state = BUZZER_OFF;
            buzzer_beep_count++;
#if defined ( POWER_SAVING )
            osal_pwrmgr_device( PWRMGR_BATTERY );
#endif

            // check to see if buzzer has beeped maximum number of times
            // if it has, then don't turn it back on
            if ( ( buzzer_beep_count < BUZZER_MAX_BEEPS ) &&
                    ( ( keyfobProximityState == KEYFOB_PROXSTATE_LINK_LOSS ) ||
                      ( keyfobProximityState == KEYFOB_PROXSTATE_PATH_LOSS )    ) )
            {
                osal_start_timerEx( keyfobapp_TaskID, KFD_TOGGLE_BUZZER_EVT, 200 );
            }
        }
        else if ( keyfobAlertState != ALERT_STATE_OFF )
        {
            // if this event was triggered while the buzzer is off then turn it on if appropriate
            keyfobapp_PerformAlert();
        }

        return (events ^ KFD_TOGGLE_BUZZER_EVT);
    }

    if ( events & KFD_TOGGLE_BUZZER_MUSIC_EVT )
    {
        // if this event was triggered while buzzer is on, turn it off, increment beep_count,
        // check whether max has been reached, and if not set the OSAL timer for next event to
        // turn buzzer back on.
        if ( buzzer_state == BUZZER_ON )
        {
            buzzerStop();
            buzzer_state = BUZZER_OFF;
            buzzer_beep_count++;
#if defined ( POWER_SAVING )
            osal_pwrmgr_device( PWRMGR_BATTERY );
#endif

            // check to see if buzzer has beeped maximum number of times
            // if it has, then don't turn it back on
            if ( ( buzzer_beep_count < 53 ) &&
                    ( ( keyfobProximityState == KEYFOB_PROXSTATE_LINK_LOSS ) ||
                      ( keyfobProximityState == KEYFOB_PROXSTATE_PATH_LOSS )    ) )
            {
                osal_start_timerEx( keyfobapp_TaskID, KFD_TOGGLE_BUZZER_MUSIC_EVT, 50 );
            }
            else
            {
                keyfobapp_StopAlert();
            }
        }
        else if ( keyfobAlertState != ALERT_STATE_OFF )
        {
            // if this event was triggered while the buzzer is off then turn it on if appropriate
            keyfobapp_PerformAlertMusic();
        }

        return (events ^ KFD_TOGGLE_BUZZER_MUSIC_EVT);
    }

#if defined ( PLUS_BROADCASTER )
    if ( events & KFD_ADV_IN_CONNECTION_EVT )
    {
        uint8 turnOnAdv = TRUE;
        // Turn on advertising while in a connection
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &turnOnAdv );
        return (events ^ KFD_ADV_IN_CONNECTION_EVT);
    }
#endif
#if defined(KNOCK_WAKE_UP)
    if ( events & KFD_KNOCK_EVT )
    {
#if 0
        HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_ON );
        HalLedSet( HAL_LED_2, HAL_LED_MODE_FLASH );
        gKnockingCount++;
        advertDataSmall[13] = (gKnockingCount >> 8) & 0xFF;
        advertDataSmall[14] = gKnockingCount & 0xFF;
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataSmall ), advertDataSmall );
        uint8 advState = TRUE;
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, gAdvIntervalMin );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, gAdvIntervalMin );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 0 );
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
        osal_start_timerEx( keyfobapp_TaskID, KFD_KNOCK_TIMER_EVT, 30000);
        osal_pwrmgr_device( PWRMGR_ALWAYS_ON );
#else
        if (gKnockingCount == 0)
        {
            osal_start_timerEx( keyfobapp_TaskID, KFD_KNOCK_TIMER_EVT, 1500);
        }
        gKnockingCount++;
        if (gKnockingCount > 5)
        {
            gHadWakeUp++;
            osal_snv_write(BLE_NVID_KNOCK_WAKE_UP, 1, &gHadWakeUp);
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_PHOBOS_NOW, 170);
        }
        /*
        HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_ON );
        HalLedSet( HAL_LED_2, HAL_LED_MODE_FLASH );
        uint8 advState = TRUE;
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, gAdvIntervalMin );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, gAdvIntervalMin );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 0 );
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
        osal_start_timerEx( keyfobapp_TaskID, KFD_KNOCK_TIMER_EVT, 2000);
        */
#endif
        return (events ^ KFD_KNOCK_EVT);
    }

    if (events & KFD_KNOCK_TIMER_EVT)
    {
        gKnockingCount = 0;
        /*
          gKnockingCount++;
          advertDataSmall[13] = (gKnockingCount >> 8) & 0xFF;
          advertDataSmall[14] = gKnockingCount & 0xFF;
          GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataSmall ), advertDataSmall );
          HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_OFF);
         */
        return (events ^ KFD_KNOCK_TIMER_EVT);
    }

    if (events & KFD_UNEXPECTED_ACTIVATE_CHECK_EVT)
    {
        uint8 ifUnexpectedActivate = 0;
        uint8 ret = osal_snv_read(BLE_NVID_UNEXPECTED_ACTIVATE_CHECK, 1, &ifUnexpectedActivate);

        if (ret == SUCCESS)
        {
            if (ifUnexpectedActivate != 0)
            {
                gHadWakeUp = 0;
                osal_snv_write(BLE_NVID_KNOCK_WAKE_UP, 1, &gHadWakeUp);
                osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_PHOBOS_NOW, 170);
            }
        }
        else
        {
            gHadWakeUp = 0;
            osal_snv_write(BLE_NVID_KNOCK_WAKE_UP, 1, &gHadWakeUp);
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_PHOBOS_NOW, 170);
        }
        return (events ^ KFD_UNEXPECTED_ACTIVATE_CHECK_EVT);
    }
#endif

#if defined(READ_INTERNAL_TEMPERATURE)
    if (events & KFD_TEST_INTERNAL_TEMPERATURE_EVT)
    {
        uint16 temperature = HalAdcTemperatureInternalRead();
        advertDataSmall[13] = (temperature >> 8) & 0xFF;
        advertDataSmall[14] = temperature & 0xFF;
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataSmall ), advertDataSmall);
        //osal_start_timerEx( keyfobapp_TaskID, KFD_TEST_INTERNAL_TEMPERATURE_EVT, 6000);
        return (events ^ KFD_TEST_INTERNAL_TEMPERATURE_EVT);
    }
#endif

    if (events & KFD_FUN_FLASH_LED)
    {
        /* Ative delay: 125 cycles ~1 msec */
        P1_1 = 1;
        KFD_HAL_DELAY(1250);
        P1_1 = 0;
        //HalLedSet(HAL_LED_1 | HAL_LED_2, HAL_LED_MODE_ON );
        //HalLedSet(HAL_LED_1 | HAL_LED_2, HAL_LED_MODE_FLASH );
        osal_start_timerEx( keyfobapp_TaskID, KFD_FUN_FLASH_LED, 3100 );
        return (events ^ KFD_FUN_FLASH_LED);
    }
    // Discard unknown events
    return 0;
}

/*********************************************************************
 * @fn      keyfobapp_ProcessOSALMsg
 *
 * @brief   Process an incoming task message.
 *
 * @param   pMsg - message to process
 *
 * @return  none
 */
static void keyfobapp_ProcessOSALMsg( osal_event_hdr_t *pMsg )
{
    switch ( pMsg->event )
    {
    case KEY_CHANGE:
        keyfobapp_HandleKeys( ((keyChange_t *)pMsg)->state, ((keyChange_t *)pMsg)->keys );
        break;
    }
}

/*********************************************************************
 * @fn      keyfobapp_HandleKeys
 *
 * @brief   Handles all key events for this device.
 *
 * @param   shift - true if in shift/alt.
 * @param   keys - bit field for key events. Valid entries:
 *                 HAL_KEY_SW_2
 *                 HAL_KEY_SW_1
 *
 * @return  none
 */
static void keyfobapp_HandleKeys( uint8 shift, uint8 keys )
{
    uint8 SK_Keys = 0;

    (void)shift;  // Intentionally unreferenced parameter

    if ( keys & HAL_KEY_SW_1 )
    {
        SK_Keys |= SK_KEY_LEFT;

        // if is active, pressing the left key should toggle
        // stop the alert
        if( keyfobAlertState != ALERT_STATE_OFF )
        {
            keyfobapp_StopAlert();
        }

        // if device is in a connection, toggle the Tx power level between 0 and
        // -6 dBm
        if( gapProfileState == GAPROLE_CONNECTED )
        {
            int8 currentTxPowerLevel;
            int8 newTxPowerLevel;

            ProxReporter_GetParameter( PP_TX_POWER_LEVEL, &currentTxPowerLevel );

            switch ( currentTxPowerLevel )
            {
            case 0:
                newTxPowerLevel = -6;
                // change power to -6 dBm
                HCI_EXT_SetTxPowerCmd( HCI_EXT_TX_POWER_MINUS_6_DBM );
                // Update Tx powerl level in Proximity Reporter (and send notification)
                // if enabled)
                ProxReporter_SetParameter( PP_TX_POWER_LEVEL, sizeof ( int8 ), &newTxPowerLevel );
                break;

            case (-6):
                newTxPowerLevel = 0;
                // change power to 0 dBm
                HCI_EXT_SetTxPowerCmd( HCI_EXT_TX_POWER_0_DBM );
                // Update Tx powerl level in Proximity Reporter (and send notification)
                // if enabled)
                ProxReporter_SetParameter( PP_TX_POWER_LEVEL, sizeof ( int8 ), &newTxPowerLevel );
                break;

            default:
                // do nothing
                break;
            }
        }

    }

    if ( keys & HAL_KEY_SW_2 )
    {

        SK_Keys |= SK_KEY_RIGHT;

        // if device is not in a connection, pressing the right key should toggle
        // advertising on and off
        if( gapProfileState != GAPROLE_CONNECTED )
        {
            uint8 current_adv_enabled_status;
            uint8 new_adv_enabled_status;

            //Find the current GAP advertisement status
            GAPRole_GetParameter( GAPROLE_ADVERT_ENABLED, &current_adv_enabled_status );

            if( current_adv_enabled_status == FALSE )
            {
                new_adv_enabled_status = TRUE;
            }
            else
            {
                new_adv_enabled_status = FALSE;
            }

            //change the GAP advertisement status to opposite of current status
            GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &new_adv_enabled_status );
        }

    }

    SK_SetParameter( SK_KEY_ATTR, sizeof ( uint8 ), &SK_Keys );
}

/*********************************************************************
 * @fn      keyfobapp_PerformAlert
 *
 * @brief   Performs an alert
 *
 * @param   none
 *
 * @return  none
 */
static void keyfobapp_PerformAlert( void )
{

    if ( keyfobProximityState == KEYFOB_PROXSTATE_LINK_LOSS )
    {
        switch( keyfobProxLLAlertLevel )
        {
        case PP_ALERT_LEVEL_LOW:

#if defined ( POWER_SAVING )
            osal_pwrmgr_device( PWRMGR_ALWAYS_ON );
#endif

            keyfobAlertState = ALERT_STATE_LOW;

            buzzerStart( BUZZER_ALERT_LOW_FREQ );
            buzzer_state = BUZZER_ON;
            // only run buzzer for 200ms
            osal_start_timerEx( keyfobapp_TaskID, KFD_TOGGLE_BUZZER_EVT, 200 );

            HalLedSet(HAL_LED_2, HAL_LED_MODE_OFF );
            break;

        case PP_ALERT_LEVEL_HIGH:

#if defined ( POWER_SAVING )
            osal_pwrmgr_device( PWRMGR_ALWAYS_ON );
#endif

            keyfobAlertState = ALERT_STATE_HIGH;

            buzzerStart( BUZZER_ALERT_HIGH_FREQ );
            buzzer_state = BUZZER_ON;
            // only run buzzer for 200ms
            osal_start_timerEx( keyfobapp_TaskID, KFD_TOGGLE_BUZZER_EVT, 200 );

            //HalLedSet( HAL_LED_1, HAL_LED_MODE_ON );
            HalLedSet( HAL_LED_2, HAL_LED_MODE_FLASH );
            break;

        case PP_ALERT_LEVEL_NO:
            // Fall through
        default:
            keyfobapp_StopAlert();
            break;
        }
    }
    else if ( keyfobProximityState == KEYFOB_PROXSTATE_PATH_LOSS )
    {
        switch( keyfobProxIMAlertLevel )
        {
        case PP_ALERT_LEVEL_LOW:

#if defined ( POWER_SAVING )
            osal_pwrmgr_device( PWRMGR_ALWAYS_ON );
#endif

            keyfobAlertState = ALERT_STATE_LOW;

            buzzerStart( BUZZER_ALERT_LOW_FREQ );
            buzzer_state = BUZZER_ON;
            // only run buzzer for 200ms
            osal_start_timerEx( keyfobapp_TaskID, KFD_TOGGLE_BUZZER_EVT, 200 );

            HalLedSet( HAL_LED_2, HAL_LED_MODE_OFF );
            break;


        case PP_ALERT_LEVEL_HIGH:

#if defined ( POWER_SAVING )
            osal_pwrmgr_device( PWRMGR_ALWAYS_ON );
#endif

            keyfobAlertState = ALERT_STATE_HIGH;

            buzzerStart( BUZZER_ALERT_HIGH_FREQ );
            buzzer_state = BUZZER_ON;
            // only run buzzer for 200ms
            osal_start_timerEx( keyfobapp_TaskID, KFD_TOGGLE_BUZZER_MUSIC_EVT, 100 );

            //HalLedSet( HAL_LED_1, HAL_LED_MODE_ON );
            //HalLedSet( HAL_LED_2, HAL_LED_MODE_FLASH );
            break;

        case PP_ALERT_LEVEL_NO:
            // Fall through
        default:
            keyfobapp_StopAlert();
            break;
        }
    }

}

/*
 4 DaDiao:
 DO: 262Hz (# 1 DO#: 277Hz)
 RE: 294Hz (# 2 RE#: 311Hz)
 M : 330Hz
 FA: 349Hz (# 4 FA#: 370Hz)
 SO: 392Hz (# 5 SO#: 415Hz)
 LA: 440Hz (# 6: 466Hz)
 SI: 494Hz

 5 DaDiao:
 DO: 523Hz (# 1 DO#: 554Hz)
 RE: 587Hz (# 2 RE#: 622Hz)
 M : 659Hz
 FA: 698Hz (# 4 FA#: 740Hz)
 SO: 784Hz (# 5 SO#: 831Hz)
 LA: 880Hz (# 6: 932Hz)
 SI: 988Hz

 6 DaDiao:
 DO: 1046Hz (# 1 DO#: 1109Hz)
 RE: 1175Hz (# 2 RE#: 1245Hz)
 M : 1318Hz
 FA: 1397Hz (# 4 FA#: 1480Hz)
 SO: 1568Hz (# 5 SO#: 1661Hz)
 LA: 1760Hz (# 6: 1865Hz)
 SI: 1976Hz
*/
// http://www.ebnar.cn/article/2012-2-10/602-1.html
typedef struct
{
    uint8 yin;
    uint8 yinSharp;
} t_struct_YIN;

typedef struct
{
    t_struct_YIN yins;
    uint8 paizi;
    uint8 dadiao;
} t_struct_song;

// Hz
static uint16 gDoReMFaSoLaSi[10][8][2] =
{
    /*0*/{{0, 0}, {16, 17}, {18, 19}, {21, 0}, {22, 23}, {25, 26}, {28, 29}, {31, 0}},
    /*1*/{{0, 0}, {33, 35}, {37, 39}, {41, 0}, {44, 46}, {49, 52}, {55, 58}, {62, 0}},
    /*2*/{{0, 0}, {65, 69}, {73, 78}, {82, 0}, {87, 92}, {98, 104}, {110, 117}, {123, 0}},

    /*3*/{{0, 0}, {131, 139}, {147, 156}, {165, 0}, {175, 185}, {196, 208}, {220, 233}, {247, 0}},
    /*4*/{{0, 0}, {262, 277}, {294, 311}, {330, 0}, {349, 370}, {392, 415}, {440, 466}, {494, 0}},
    /*5*/{{0, 0}, {523, 554}, {587, 622}, {659, 0}, {698, 740}, {784, 831}, {880, 932}, {988, 0}},

    /*6*/{{0, 0}, {1047, 1109}, {1175, 1245}, {1319, 0}, {1397, 1480}, {1568, 1661}, {1760, 1865}, {1976, 0}},
    /*7*/{{0, 0}, {2093, 2218}, {2349, 2489}, {2637, 0}, {2794, 2960}, {3136, 3322}, {3520, 3729}, {3951, 0}},
    /*8*/{{0, 0}, {4186, 4435}, {4699, 4978}, {5274, 0}, {5588, 5920}, {6272, 6645}, {7040, 7459}, {7902, 0}},
    /*9*/{{0, 0}, {8372, 8870}, {9397, 9956}, {10548, 0}, {11175, 11840}, {12544, 13290}, {14080, 14917}, {15804, 0}},
};
#define DIAO_BASE_LOW 7
#define DIAO_BASE 8
#define DIAO_BASE_HIGH 9
static t_struct_song gMusicArray[] =
{
    {{0, 0}, 0, 0},
    {{6, 0}, 2, DIAO_BASE_LOW},
    {{6, 0}, 4, DIAO_BASE},
    {{5, 0}, 4, DIAO_BASE},
    {{3, 0}, 2, DIAO_BASE},
    {{2, 0}, 2, DIAO_BASE},

    {{1, 0}, 0, DIAO_BASE},

    {{3, 0}, 2, DIAO_BASE_LOW},
    {{3, 0}, 4, DIAO_BASE},
    {{2, 0}, 4, DIAO_BASE},
    {{1, 0}, 2, DIAO_BASE},
    {{6, 0}, 2, DIAO_BASE_LOW},

    {{5, 0}, 0, DIAO_BASE_LOW},

    {{5, 0}, 2, DIAO_BASE_LOW - 1},
    {{5, 0}, 4, DIAO_BASE_LOW},
    {{6, 0}, 4, DIAO_BASE_LOW},
    {{5, 0}, 2, DIAO_BASE_LOW},
    {{6, 0}, 2, DIAO_BASE_LOW},

    {{1, 0}, 2, DIAO_BASE_LOW},
    {{1, 0}, 4, DIAO_BASE},
    {{2, 0}, 4, DIAO_BASE},
    {{3, 0}, 2, DIAO_BASE},
    {{5, 0}, 2, DIAO_BASE},

    {{6, 0}, 2, DIAO_BASE_LOW},
    {{6, 0}, 4, DIAO_BASE},
    {{5, 0}, 4, DIAO_BASE},
    {{3, 0}, 4, DIAO_BASE},
    {{2, 0}, 4, DIAO_BASE},
    {{1, 0}, 2, DIAO_BASE},

    {{2, 0}, 0, DIAO_BASE},

    {{6, 0}, 2, DIAO_BASE_LOW},
    {{6, 0}, 4, DIAO_BASE},
    {{5, 0}, 4, DIAO_BASE},
    {{3, 0}, 2, DIAO_BASE},
    {{2, 0}, 2, DIAO_BASE},

    {{1, 0}, 0, DIAO_BASE},

    {{3, 0}, 2, DIAO_BASE_LOW},
    {{3, 0}, 4, DIAO_BASE},
    {{2, 0}, 4, DIAO_BASE},
    {{1, 0}, 2, DIAO_BASE},
    {{6, 0}, 2, DIAO_BASE_LOW},

    {{5, 0}, 0, DIAO_BASE_LOW},

    {{3, 0}, 4, DIAO_BASE_LOW - 1},
    {{3, 0}, 4, DIAO_BASE_LOW},
    {{3, 0}, 4, DIAO_BASE_LOW},
    {{5, 0}, 4, DIAO_BASE_LOW},
    {{5, 0}, 2, DIAO_BASE_LOW},
    {{6, 0}, 2, DIAO_BASE_LOW},

    {{1, 0}, 4, DIAO_BASE_LOW},
    {{1, 0}, 4, DIAO_BASE},
    {{1, 0}, 4, DIAO_BASE},
    {{2, 0}, 4, DIAO_BASE},
    {{3, 0}, 2, DIAO_BASE},
    {{5, 0}, 2, DIAO_BASE},
};
#define QUAN_YIN 500
static uint16 gPaiZi[] =
{
    QUAN_YIN, QUAN_YIN / 1, QUAN_YIN / 2, 3, QUAN_YIN / 4, 5, 6, 7, QUAN_YIN / 8 //ms
};

static void keyfobapp_PerformAlertMusic( )
{
#if defined ( POWER_SAVING )
    osal_pwrmgr_device( PWRMGR_ALWAYS_ON );
#endif

    keyfobAlertState = ALERT_STATE_HIGH;

    buzzerStart( gDoReMFaSoLaSi[gMusicArray[buzzer_beep_count].dadiao][gMusicArray[buzzer_beep_count].yins.yin][gMusicArray[buzzer_beep_count].yins.yinSharp]);
    buzzer_state = BUZZER_ON;
    // 100ms means 1/4 rhythm.
    osal_start_timerEx( keyfobapp_TaskID, KFD_TOGGLE_BUZZER_MUSIC_EVT, gPaiZi[gMusicArray[buzzer_beep_count].paizi] );

    //HalLedSet( HAL_LED_1, HAL_LED_MODE_ON );
    HalLedSet( HAL_LED_2, HAL_LED_MODE_FLASH );
}

/*********************************************************************
 * @fn      keyfobapp_StopAlert
 *
 * @brief   Stops an alert
 *
 * @param   none
 *
 * @return  none
 */
void keyfobapp_StopAlert( void )
{

    keyfobAlertState = ALERT_STATE_OFF;

    buzzerStop();
    buzzer_state = BUZZER_OFF;
    HalLedSet(HAL_LED_2, HAL_LED_MODE_OFF );


#if defined ( POWER_SAVING )
    osal_pwrmgr_device( PWRMGR_BATTERY );
#endif
}

/*********************************************************************
 * @fn      phobosRateBattCB
 *
 * @brief   Callback function for battery service.
 *
 * @param   event - service event
 *
 * @return  none
 */
static void phobosRateBattCB(uint8 event)
{
    if (event == BATT_LEVEL_NOTI_ENABLED)
    {
        // if connected start periodic measurement
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            osal_start_timerEx( keyfobapp_TaskID, KFD_BATTERY_CHECK_EVT, BATTERY_CHECK_PERIOD );
        }
        //HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_BLINK );
    }
    else if (event == BATT_LEVEL_NOTI_DISABLED)
    {
        // stop periodic measurement
        osal_stop_timerEx( keyfobapp_TaskID, KFD_BATTERY_CHECK_EVT );
    }
}

/*********************************************************************
 * @fn      peripheralStateNotificationCB
 *
 * @brief   Notification from the profile of a state change.
 *
 * @param   newState - new state
 *
 * @return  none
 */
static void peripheralStateNotificationCB( gaprole_States_t newState )
{
    //  uint16 connHandle = INVALID_CONNHANDLE;
    uint8 valFalse = FALSE;

    if ( gapProfileState != newState )
    {
        if (gapProfileState == GAPROLE_CONNECTED &&
                newState != GAPROLE_CONNECTED)
        {
            uint8 advState = TRUE;

            // link loss timeout-- use fast advertising
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, gAdvIntervalMin );
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, gAdvIntervalMin );
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 15000 );

            // Enable advertising
            GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
            //HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_ON );
        }
        // if advertising stopped
        else if ( gapProfileState == GAPROLE_ADVERTISING &&
                  newState == GAPROLE_WAITING )
        {
#if defined(KNOCK_WAKE_UP)
            if (gHadWakeUp)
            {
                #if 0
                uint8 advState = TRUE;
                GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, gAdvIntervalMax );
                GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, gAdvIntervalMax );
                GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 0 );
                GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
                osal_start_timerEx( keyfobapp_TaskID, KFD_ADV_BEACON_NORMAL_EVT, 15000 );
                #endif
                HalLedSet(HAL_LED_2, HAL_LED_MODE_OFF );
                osal_start_timerEx( keyfobapp_TaskID, KFD_FUN_FLASH_LED, 3100 );
            }
            else
            {
                //Go into PM3
                HalLedSet(HAL_LED_2, HAL_LED_MODE_OFF );
            }
#else
            uint8 advState = TRUE;
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, gAdvIntervalMax );
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, gAdvIntervalMax );
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 0 );
            GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
            HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_OFF );
            osal_start_timerEx( keyfobapp_TaskID, KFD_ADV_BEACON_NORMAL_EVT, 15000 );
#endif
        }

        switch( newState )
        {
        case GAPROLE_STARTED:
        {
            // Set the system ID from the bd addr
            uint8 systemId[DEVINFO_SYSTEM_ID_LEN];
            uint8 systemIdBak[DEVINFO_SYSTEM_ID_LEN];
            GAPRole_GetParameter(GAPROLE_BD_ADDR, systemId);
            //osal_memcpy(&advertDataBeacon[25], systemId, B_ADDR_LEN);
            osal_memcpy(&advertDataSmall[7], systemId, B_ADDR_LEN);
            osal_memcpy(systemIdBak, systemId, DEVINFO_SYSTEM_ID_LEN);

            // shift three bytes up
            systemId[7] = systemId[5];
            systemId[6] = systemId[4];
            systemId[5] = systemId[3];

            // set middle bytes to zero
            systemId[4] = 0;
            systemId[3] = 0;
            DevInfo_SetParameter(DEVINFO_SYSTEM_ID, DEVINFO_SYSTEM_ID_LEN, systemId);

            // Set the serial number from the bd addr.
            uint8 serialNumber[DEVINFO_SERIAL_NUMBER_LEN + 2] = "\0";
            uint8 aNumber;
            uint8 j = 0;
            for (int8 i = B_ADDR_LEN - 1; i >= 0; i--)
            {
                aNumber = systemIdBak[i];
                if (aNumber < 10)
                {
                    strcat((char *)serialNumber + j * 2, (const char *)"0");
                    _itoa(aNumber, serialNumber + j * 2 + 1, 16);
                }
                else
                {
                    _itoa(aNumber, serialNumber + j * 2, 16);
                }

                /*if (osal_memcmp(&aNumber, &Zero, 1) == TRUE)
                {
                    strcat((char*)serialNumber+j*2+1, (const char*)"0");
                }*/
                j++;
            }
            DevInfo_SetParameter(DEVINFO_SERIAL_NUMBER, DEVINFO_SERIAL_NUMBER_LEN, serialNumber);
        }
        break;

        //if the state changed to connected, initially assume that keyfob is in range
        case GAPROLE_ADVERTISING:
        {
            // Visual feedback that we are advertising.
            //HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_ON );
        }
        break;

        //if the state changed to connected, initially assume that keyfob is in range
        case GAPROLE_CONNECTED:
        {
            uint8 ifUnexpectedActivate = 0;
            osal_snv_write(BLE_NVID_UNEXPECTED_ACTIVATE_CHECK, 1, &ifUnexpectedActivate);

            // set the proximity state to either path loss alert or in range depending
            // on the value of keyfobProxIMAlertLevel (which was set by proximity monitor)
            if( keyfobProxIMAlertLevel != PP_ALERT_LEVEL_NO )
            {
                keyfobProximityState = KEYFOB_PROXSTATE_PATH_LOSS;
                // perform alert
                keyfobapp_PerformAlert();
                buzzer_beep_count = 0;
            }
            else // if keyfobProxIMAlertLevel == PP_ALERT_LEVEL_NO
            {
                keyfobProximityState = KEYFOB_PROXSTATE_CONNECTED_IN_RANGE;
                keyfobapp_StopAlert();
            }

            //        GAPRole_GetParameter( GAPROLE_CONNHANDLE, &connHandle );

#if defined ( PLUS_BROADCASTER )
            osal_start_timerEx( keyfobapp_TaskID, KFD_ADV_IN_CONNECTION_EVT, ADV_IN_CONN_WAIT );
#endif

            // Turn off LED that shows we're advertising
            HalLedSet(HAL_LED_2, HAL_LED_MODE_OFF );

            // Set timer to update connection parameters
            // 5 seconds should allow enough time for Service Discovery by the collector to finish
            osal_start_timerEx( keyfobapp_TaskID, KFD_CONN_PARAM_UPDATE_EVT, 5000);
        }
        break;

        case GAPROLE_WAITING:
        {
            // then the link was terminated intentionally by the slave or master,
            // or advertising timed out
            keyfobProximityState = KEYFOB_PROXSTATE_INITIALIZED;

            // Turn off immediate alert
            ProxReporter_SetParameter(PP_IM_ALERT_LEVEL, sizeof(valFalse), &valFalse);
            keyfobProxIMAlertLevel = PP_ALERT_LEVEL_NO;

            // Change attribute value of Accelerometer Enable to FALSE
            //Accel_SetParameter(ACCEL_ENABLER, sizeof(valFalse), &valFalse);
            // Stop the acceleromter
            //accelEnablerChangeCB(); // SetParameter does not trigger the callback

            // Turn off LED that shows we're advertising
            //        HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_OFF );

            // Stop alert if it was active
            if( keyfobAlertState != ALERT_STATE_OFF )
            {
                keyfobapp_StopAlert();
            }
        }
        break;

        case GAPROLE_WAITING_AFTER_TIMEOUT:
        {
            // the link was dropped due to supervision timeout
            keyfobProximityState = KEYFOB_PROXSTATE_LINK_LOSS;

            // Turn off immediate alert
            ProxReporter_SetParameter(PP_IM_ALERT_LEVEL, sizeof(valFalse), &valFalse);
            keyfobProxIMAlertLevel = PP_ALERT_LEVEL_NO;

            // Change attribute value of Accelerometer Enable to FALSE
            //Accel_SetParameter(ACCEL_ENABLER, sizeof(valFalse), &valFalse);
            // Stop the acceleromter
            //accelEnablerChangeCB(); // SetParameter does not trigger the callback

            // Perform link loss alert if enabled
            if( keyfobProxLLAlertLevel != PP_ALERT_LEVEL_NO )
            {
                keyfobapp_PerformAlert();
                buzzer_beep_count = 0;
            }
        }
        break;

        default:
            // do nothing
            break;
        }
    }

    gapProfileState = newState;
}

/*********************************************************************
 * @fn      proximityAttrCB
 *
 * @brief   Notification from the profile of an atrribute change by
 *          a connected device.
 *
 * @param   attrParamID - Profile's Attribute Parameter ID
 *            PP_LINK_LOSS_ALERT_LEVEL  - The link loss alert level value
 *            PP_IM_ALERT_LEVEL  - The immediate alert level value
 *
 * @return  none
 */
static void proximityAttrCB( uint8 attrParamID )
{
    switch( attrParamID )
    {

    case PP_LINK_LOSS_ALERT_LEVEL:
        ProxReporter_GetParameter( PP_LINK_LOSS_ALERT_LEVEL, &keyfobProxLLAlertLevel );
        break;

    case PP_IM_ALERT_LEVEL:
    {
        ProxReporter_GetParameter( PP_IM_ALERT_LEVEL, &keyfobProxIMAlertLevel );

        // if proximity monitor set the immediate alert level to low or high, then
        // the monitor calculated that the path loss to the keyfob (proximity observer)
        // has exceeded the threshold
        if( keyfobProxIMAlertLevel != PP_ALERT_LEVEL_NO )
        {
            keyfobProximityState = KEYFOB_PROXSTATE_PATH_LOSS;
            keyfobapp_PerformAlert();
            buzzer_beep_count = 0;
        }
        else // proximity monitor turned off alert because the path loss is below threshold
        {
            keyfobProximityState = KEYFOB_PROXSTATE_CONNECTED_IN_RANGE;
            keyfobapp_StopAlert();
        }
    }
    break;

    default:
        // should not reach here!
        break;
    }

}

/*********************************************************************
 * @fn      ccServiceChangeCB
 *
 * @brief   Callback from Connection Control indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
static void ccServiceChangeCB( uint8 paramID )
{

    // CCSERVICE_CHAR1: read & notify only

    // CCSERVICE_CHAR: requested connection parameters
    if( paramID == CCSERVICE_CHAR2 )
    {
        uint8 buf[CCSERVICE_CHAR2_LEN];
        uint16 minConnInterval;
        uint16 maxConnInterval;
        uint16 slaveLatency;
        uint16 timeoutMultiplier;

        CcService_GetParameter( CCSERVICE_CHAR2, buf );

        minConnInterval = BUILD_UINT16(buf[0], buf[1]);
        maxConnInterval = BUILD_UINT16(buf[2], buf[3]);
        slaveLatency = BUILD_UINT16(buf[4], buf[5]);
        timeoutMultiplier = BUILD_UINT16(buf[6], buf[7]);

        //osal_start_timerEx( keyfobapp_TaskID, KFD_CONN_PARAM_UPDATE_EVT, 5000);
        // Update connection parameters
        GAPRole_SendUpdateParam( minConnInterval, maxConnInterval,
                                 slaveLatency, timeoutMultiplier, GAPROLE_NO_ACTION);
    }

    // CCSERVICE_CHAR3: Disconnect request
    if( paramID == CCSERVICE_CHAR3 )
    {
        // Any change in the value will terminate the connection
        GAPRole_TerminateConnection();
    }
}


static void glieseServiceChangeCB( uint8 paramID )
{
    if ( paramID == GLIESEPROFILE_CHAR3 )
    {
        uint8 buf[GAP_DEVICE_NAME_LEN] = {0};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR3, buf);
        //GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, attDeviceName );
        GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN - 3, buf );
        osal_snv_write(BLE_NVID_PHOBOS_DEVICE_NAME, GAP_DEVICE_NAME_LEN, buf);
        return;
    }
    if ( paramID == GLIESEPROFILE_CHAR1 )
    {
        uint8 beacon[GLIESEPROFILE_CHAR1_LEN] = {0};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR1, beacon);
        uint8 password[4] = {0xFF,0x02,0x9D,0x8E};
        if (FALSE == osal_memcmp(password, beacon+7, 4))
        {
            return;
        }
        // set major
        advertDataBeacon[25] = beacon[0];
        advertDataBeacon[26] = beacon[1];

        // set minor
        advertDataBeacon[27] = beacon[2];
        advertDataBeacon[28] = beacon[3];
        GlieseProfile_SetParameter(GLIESEPROFILE_CHAR1, GLIESEPROFILE_CHAR1_LEN, beacon);
        // set The 2's complement of the calibrated Tx Power
        advertDataBeacon[29] = beacon[4];
#if 0
        //Tx Power
        uint8 txPower = HCI_EXT_TX_POWER_0_DBM;
        switch (beacon[5])
        {
        case 0:
            txPower = HCI_EXT_TX_POWER_MINUS_23_DBM;
            break;

        case 1:
            txPower = HCI_EXT_TX_POWER_MINUS_6_DBM;
            break;

        case 2:
            txPower = HCI_EXT_TX_POWER_0_DBM;
            break;

        case 3:
            txPower = HCI_EXT_TX_POWER_4_DBM;
            break;
        }
        HCI_EXT_SetTxPowerCmd( txPower );
#endif
        //Advertising Freq (1-100) Unit: 100ms
        if (beacon[6] != 0)
        {
            gAdvIntervalMin = (uint16)((float)beacon[6] * (float)100 / (float)0.625);
            gAdvIntervalMax = gAdvIntervalMin;
        }
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );

        osal_snv_write(BLE_NVID_iBeacon_SET1, GLIESEPROFILE_CHAR1_LEN-4, beacon);
        return;
    }

#if defined(KNOCK_WAKE_UP)
    if ( paramID == GLIESEPROFILE_CHAR6 )
    {
        uint8 buffer[GLIESEPROFILE_CHAR6_LEN] = {0};
        uint8 password[6] = {0x60,0x78,0xB6,0xA0,0x94,0x15};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR6, buffer);
        if (TRUE == osal_memcmp(buffer, password, GLIESEPROFILE_CHAR6_LEN-1))
        {
            if (buffer[6] == 0)
            {
                osal_snv_write(BLE_NVID_KNOCK_WAKE_UP, 1, &buffer[6]);
                uint8 ifUnexpectedActivate = 1;
                osal_snv_write(BLE_NVID_UNEXPECTED_ACTIVATE_CHECK, 1, &ifUnexpectedActivate);
                osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_PHOBOS_NOW, 1000);
            }
        }
        return;
    }
#endif
    if ( paramID == GLIESEPROFILE_CHAR10 )
    {
        uint8 buffer[GLIESEPROFILE_CHAR10_LEN] = {0};
        uint8 password[6] = {0x61,0x88,0xC6,0xA1,0xA4,0x16};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR10, buffer);
        if (TRUE == osal_memcmp(buffer, password, 6))
        {
            uint32 passkey = ((uint32)buffer[6]<<24)|
                ((uint32)buffer[7]<<16)|
                ((uint32)buffer[8]<<8)|
                ((uint32)buffer[9]);//DEFAULT_PHOBOS_PASSCODE;
            uint8 pairMode = buffer[10];//GAPBOND_PAIRING_MODE_WAIT_FOR_REQ;
            uint8 mitm = buffer[11];//TRUE;
            uint8 ioCap = buffer[12];//GAPBOND_IO_CAP_DISPLAY_ONLY;
            uint8 bonding = buffer[13];//TRUE;

            gDefaultPasscode = passkey;
            GAPBondMgr_SetParameter( GAPBOND_DEFAULT_PASSCODE, sizeof ( uint32 ), &passkey );
            GAPBondMgr_SetParameter( GAPBOND_PAIRING_MODE, sizeof ( uint8 ), &pairMode );
            GAPBondMgr_SetParameter( GAPBOND_MITM_PROTECTION, sizeof ( uint8 ), &mitm );
            GAPBondMgr_SetParameter( GAPBOND_IO_CAPABILITIES, sizeof ( uint8 ), &ioCap );
            GAPBondMgr_SetParameter( GAPBOND_BONDING_ENABLED, sizeof ( uint8 ), &bonding );

            osal_snv_write(BLE_NVID_BOND_SETTING, 8, &buffer[6]);
        }
        return;
    }
#if defined(iBeacon)
    if (paramID == GLIESEPROFILE_CHAR2)
    {
        uint8 beacon[GLIESEPROFILE_CHAR2_LEN] = {0};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR2, beacon);
        osal_memcpy(advertDataBeacon + 9, beacon, 16);
        osal_snv_write(BLE_NVID_iBeacon_SET, GLIESEPROFILE_CHAR2_LEN, beacon);
        return;
    }
#endif
#if defined(READ_INTERNAL_TEMPERATURE)
    if ( paramID == GLIESEPROFILE_CHAR7 )
    {
        uint8 buffer[GLIESEPROFILE_CHAR7_LEN];
        uint8 password[4] = {0x11,0x33,0xC9,0xFF};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR7, buffer);
        if (TRUE == osal_memcmp(buffer, password, 4))
        {
            int8 chazhi = buffer[8];
            osal_snv_write(BLE_NVID_CC2541_INTERNAL_TEMPERATURE_CALIBRATION, 1, &chazhi);
        }
    }
#endif
}
#if defined(DEIMOS_TEMPERATURE)
static void tmp112ServiceChangeCB( uint8 paramID )
{
    if ( paramID == TMP112PROFILE_CHAR1 )
    {
        uint8 aryVal[TMP112PROFILE_CHAR1_LEN] = {0};
        Tmp112Profile_GetParameter(TMP112PROFILE_CHAR1, aryVal);
        uint8 password[4] = {0xFF,0x02,0x9D,0x8E};
        if (FALSE == osal_memcmp(password, aryVal+2, 4))
        {
            uint8 mass[TMP112PROFILE_CHAR1_LEN] = {0xF2, 0xF3, 0x14, 0x15, 0x16, 0x17};
            Tmp112Profile_SetParameter(TMP112PROFILE_CHAR1, TMP112PROFILE_CHAR1_LEN, mass);
            return;
        }
        uint8 mass[TMP112PROFILE_CHAR1_LEN] = {0x12, 0x13, 0x14, 0x15, 0x16, 0x17};
        Tmp112Profile_SetParameter(TMP112PROFILE_CHAR1, TMP112PROFILE_CHAR1_LEN, mass);

        uint16 seconds = (uint16)(aryVal[0] << 8 | aryVal[1]);
        gTemperatureCheckInterval = seconds * 1000;
        osal_snv_write(BLE_NVID_TEMPERATURE_CHECK_INTERVAL, TMP112PROFILE_CHAR1_LEN-4, aryVal);
        return;
    }
}

static void readTMP112Data( void )
{
    //static bool heihei = TRUE;
    uint8 tData[TMP112_DATA_LEN];

    if (Tmp112Read(tData))
    {
        advertDataSmall[13] = tData[0];
        advertDataSmall[14] = tData[1];
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataSmall ), advertDataSmall );
        /*
        if (heihei)
        {
            GlieseProfile_SetParameter(GLIESEPROFILE_CHAR4, 1, &tData[1]);
        }
        else
        {
            GlieseProfile_SetParameter(GLIESEPROFILE_CHAR4, 1, &tData[0]);
        }
        heihei = !heihei;
        */
    }
}
#endif

//绑定过程中的密码管理回调函数
static void ProcessPasscodeCB(uint8 *deviceAddr, uint16 connectionHandle,
                              uint8 uiInputs, uint8 uiOutputs )
{
    //uint32  passcode;
    //uint8   str[7];

    //在这里可以设置存储，保存之前设定的密码，这样就可以动态修改配对密码了。
    // Create random passcode
    /*LL_Rand( ((uint8 *) &passcode), sizeof( uint32 ));
    passcode %= 1000000;
    */

    //在lcd上显示当前的密码，这样手机端，根据此密码连接。
    // Display passcode to user
    /*if ( uiOutputs != 0 )
    {
      HalLcdWriteString( "Passcode:",  HAL_LCD_LINE_1 );
      HalLcdWriteString( (char *) _ltoa(passcode, str, 10),  HAL_LCD_LINE_2 );
    }*/

    // Send passcode response
    GAPBondMgr_PasscodeRsp( connectionHandle, SUCCESS, gDefaultPasscode);
}

//绑定过程中的状态管理，在这里可以设置标志位，当密码不正确时不允许连接。
static void ProcessPairStateCB( uint16 connHandle, uint8 state, uint8 status )
{
    if ( state == GAPBOND_PAIRING_STATE_STARTED )/*主机发起连接，会进入开始绑定状态*/
    {
        //HalLcdWriteString( "Pairing started", HAL_LCD_LINE_1 );
        gPairStatus = 0;
        uint8 ifUnexpectedActivate = 0;
        osal_snv_write(BLE_NVID_UNEXPECTED_ACTIVATE_CHECK, 1, &ifUnexpectedActivate);
    }
    else if ( state == GAPBOND_PAIRING_STATE_COMPLETE )/*当主机提交密码后，会进入完成*/
    {
        if ( status == SUCCESS )
        {
            //HalLcdWriteString( "Pairing success", HAL_LCD_LINE_1 );/*密码正确*/
            gPairStatus = 1;
        }
        else
        {
            //HalLcdWriteStringValue( "Pairing fail", status, 10, HAL_LCD_LINE_1 );/*密码不正确，或者先前已经绑定*/
            if(status == 8) /*已绑定*/
            {
                gPairStatus = 1;
            }
            else
            {
                gPairStatus = 0;
            }
        }
        //判断配对结果，如果不正确立刻停止连接。
        if(gapProfileState == GAPROLE_CONNECTED && gPairStatus != 1)
        {
            GAPRole_TerminateConnection();
        }
    }
    else if ( state == GAPBOND_PAIRING_STATE_BONDED )
    {
        if ( status == SUCCESS )
        {
            //HalLcdWriteString( "Bonding success", HAL_LCD_LINE_1 );
        }
    }

}

/*********************************************************************
 * @fn      accelEnablerChangeCB
 *
 * @brief   Called by the Accelerometer Profile when the Enabler Attribute
 *          is changed.
 *
 * @param   none
 *
 * @return  none
 */
/*
static void accelEnablerChangeCB( void )
{
  bStatus_t status = Accel_GetParameter( ACCEL_ENABLER, &accelEnabler );

  if (status == SUCCESS){
    if (accelEnabler)
    {
      // Initialize accelerometer
      accInit();
      // Setup timer for accelerometer task
      osal_start_timerEx( keyfobapp_TaskID, KFD_ACCEL_READ_EVT, ACCEL_READ_PERIOD );
    } else
    {
      // Stop the acceleromter
      accStop();
      osal_stop_timerEx( keyfobapp_TaskID, KFD_ACCEL_READ_EVT);
    }
  } else
  {
    //??
  }
}
*/

/*********************************************************************
 * @fn      accelRead
 *
 * @brief   Called by the application to read accelerometer data
 *          and put data in accelerometer profile
 *
 * @param   none
 *
 * @return  none
 */
/*
static void accelRead( void )
{

  static int8 x, y, z;
  int8 new_x, new_y, new_z;

  // Read data for each axis of the accelerometer
  accReadAcc(&new_x, &new_y, &new_z);

  // Check if x-axis value has changed by more than the threshold value and
  // set profile parameter if it has (this will send a notification if enabled)
  if( (x < (new_x-ACCEL_CHANGE_THRESHOLD)) || (x > (new_x+ACCEL_CHANGE_THRESHOLD)) )
  {
    x = new_x;
    Accel_SetParameter(ACCEL_X_ATTR, sizeof ( int8 ), &x);
  }

  // Check if y-axis value has changed by more than the threshold value and
  // set profile parameter if it has (this will send a notification if enabled)
  if( (y < (new_y-ACCEL_CHANGE_THRESHOLD)) || (y > (new_y+ACCEL_CHANGE_THRESHOLD)) )
  {
    y = new_y;
    Accel_SetParameter(ACCEL_Y_ATTR, sizeof ( int8 ), &y);
  }

  // Check if z-axis value has changed by more than the threshold value and
  // set profile parameter if it has (this will send a notification if enabled)
  if( (z < (new_z-ACCEL_CHANGE_THRESHOLD)) || (z > (new_z+ACCEL_CHANGE_THRESHOLD)) )
  {
    z = new_z;
    Accel_SetParameter(ACCEL_Z_ATTR, sizeof ( int8 ), &z);
  }

}
*/
/*********************************************************************
*********************************************************************/
