/**************************************************************************************************
  Filename:       i2c.h
  Revised:        $Date: 2015-06-10 10:04:58 +0800 (Wed, 10 Jun 2015) $
  Revision:       $Revision: 0 $


**************************************************************************************************/

#ifndef I2C_H
#define I2C_H

#include "hal_types.h"
#define ASM_DELAY asm("NOP");\
        asm("NOP");\
        asm("NOP");\
        asm("NOP");\
        asm("NOP");\
        asm("NOP");\
        asm("NOP");\
        asm("NOP");\
        asm("NOP");\
        asm("NOP");

#define TMP112_DATA_LEN   2

typedef enum
{
    TMP112_OFF,               // IR Temperature Sleeping
    TMP112_DATA_READY         // IR Temperature On, Configured and Data is Ready
} TMP112_States_t;

// Function prototypes
extern int8 EggReadAllLM75ATemp(uint8 *pBuf);
extern void EggLM75ATempInit(void);
extern bool EggLM75ATempRead(uint8 id, uint8 *pBuf);

extern void tempinits(void);
extern void Tmp112Init(void);
extern TMP112_States_t Tem112Status(void);
extern void TMP112OneShot(void);
extern void TMP112OneShotFinished(void);
extern bool Tmp112Read(uint8 *pBuf);
#endif
