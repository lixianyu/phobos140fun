/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "OSAL.h"
#include "linkdb.h"
#include "att.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "gapbondmgr.h"
#include "gapgattserver.h"
#include "gliese.h"
#include "hal_adc.h"
/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

#define SERVAPP_NUM_ATTR_SUPPORTED        32

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
// Gliese GATT Profile Service UUID: 0xFFF0
CONST uint8 glieseProfileServUUID[ATT_UUID_SIZE] =
{
    GLIESEPROFILE_SERV_UUID
};

// Characteristic 1 UUID: 0xFFF1
CONST uint8 glieseProfilechar1UUID[ATT_UUID_SIZE] =
{
    GLIESEPROFILE_CHAR1_UUID
};

// Characteristic 2 UUID: 0xFFF2
CONST uint8 glieseProfilechar2UUID[ATT_UUID_SIZE] =
{
    GLIESEPROFILE_CHAR2_UUID
};

// Characteristic 3 UUID: 0xFFF3
CONST uint8 glieseProfilechar3UUID[ATT_UUID_SIZE] =
{
    GLIESEPROFILE_CHAR3_UUID
};

// Characteristic 4 UUID: 0xFFF4
CONST uint8 glieseProfilechar4UUID[ATT_UUID_SIZE] =
{
    GLIESEPROFILE_CHAR4_UUID
};

// Characteristic 5 UUID: 0xFFF5
CONST uint8 glieseProfilechar5UUID[ATT_UUID_SIZE] =
{
    GLIESEPROFILE_CHAR5_UUID
};

// Characteristic 6 UUID
CONST uint8 glieseProfilechar6UUID[ATT_UUID_SIZE] =
{
    GLIESEPROFILE_CHAR6_UUID
};

// Characteristic 7 UUID
CONST uint8 glieseProfilechar7UUID[ATT_UUID_SIZE] =
{
    GLIESEPROFILE_CHAR7_UUID
};

// Characteristic 8 UUID
CONST uint8 glieseProfilechar8UUID[ATT_UUID_SIZE] =
{
    GLIESEPROFILE_CHAR8_UUID
};
// Characteristic 9 UUID
CONST uint8 glieseProfilechar9UUID[ATT_UUID_SIZE] =
{
    GLIESEPROFILE_CHAR9_UUID
};
// Characteristic 10 UUID
CONST uint8 glieseProfilechar10UUID[ATT_UUID_SIZE] =
{
    GLIESEPROFILE_CHAR10_UUID
};

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

static glieseProfileCBs_t *glieseProfile_AppCBs = NULL;

/*********************************************************************
 * Profile Attributes - variables
 */

// Gliese Profile Service attribute
static CONST gattAttrType_t glieseProfileService = { ATT_UUID_SIZE, glieseProfileServUUID };


// Gliese Profile Characteristic 1 Properties
static uint8 glieseProfileChar1Props = GATT_PROP_WRITE | GATT_PROP_READ;
// Characteristic 1 Value -- Set major and minor.
static uint8 glieseProfileChar1[GLIESEPROFILE_CHAR1_LEN] = {0};
// Gliese Profile Characteristic 1 User Description
static uint8 glieseProfileChar1UserDesp[12] = "Betelgeuse\0";


// Gliese Profile Characteristic 2 Properties
static uint8 glieseProfileChar2Props = GATT_PROP_WRITE;
// Characteristic 2 Value
/* Togather is 29 bytes:
e2 c5 6d b5 df fb 48 d2 b0 60 d0 f5 a7 10 96 e0 # iBeacon profileUUID
00 00 # major (LSB first)
00 00 # minor (LSB first)
c5 # The 2's complement of the calibrated Tx Power
00 # Tx Power
09 # Advertising Freq (1-100) Unit: 100ms
36 32 33 37 37 33 # 6 bytes passward
 */
static uint8 glieseProfileChar2[GLIESEPROFILE_CHAR2_LEN] = {0};
// Gliese Profile Characteristic 2 User Description
static uint8 glieseProfileChar2UserDesp[5] = "Vega\0";


// Gliese Profile Characteristic 3 Properties
static uint8 glieseProfileChar3Props = GATT_PROP_WRITE;
// Characteristic 3 Value -- Set device name.
static uint8 glieseProfileChar3[GAP_DEVICE_NAME_LEN] = {0};
// Gliese Profile Characteristic 3 User Description
static uint8 glieseProfileChar3UserDesp[7] = "Altair\0";


// Gliese Profile Characteristic 4 Properties
static uint8 glieseProfileChar4Props = GATT_PROP_NOTIFY;
// Characteristic 4 Value
static uint8 glieseProfileChar4 = 0;
// Gliese Profile Characteristic 4 Configuration Each client has its own
// instantiation of the Client Characteristic Configuration. Reads of the
// Client Characteristic Configuration only shows the configuration for
// that client and writes only affect the configuration of that client.
static gattCharCfg_t glieseProfileChar4Config[GATT_MAX_NUM_CONN];
// Gliese Profile Characteristic 4 User Description
static uint8 glieseProfileChar4UserDesp[6] = "Deneb\0";


// Gliese Profile Characteristic 5 Properties
static uint8 glieseProfileChar5Props = GATT_PROP_READ;
// Characteristic 5 Value
static uint8 glieseProfileChar5[GLIESEPROFILE_CHAR5_LEN] = { 0, 0, 0, 0, 0 };
// Gliese Profile Characteristic 5 User Description
static uint8 glieseProfileChar5UserDesp[8] = "Antares\0";

// Gliese Profile Characteristic 6 Properties
static uint8 glieseProfileChar6Props = GATT_PROP_WRITE_NO_RSP;
// Characteristic 6 Value --- Knock wake up.
static uint8 glieseProfileChar6[GLIESEPROFILE_CHAR6_LEN] = { 0 };
// Gliese Profile Characteristic 6 User Description
static uint8 glieseProfileChar6UserDesp[7] = "Bianca\0";

// Gliese Profile Characteristic 7 Properties
static uint8 glieseProfileChar7Props = GATT_PROP_READ | GATT_PROP_WRITE_NO_RSP;
// Characteristic 7 Value
// The first four bytes is passcode
static uint8 glieseProfileChar7[GLIESEPROFILE_CHAR7_LEN] = {0};
// Gliese Profile Characteristic 7 User Description
static uint8 glieseProfileChar7UserDesp[8] = "Capella\0";

// Gliese Profile Characteristic 8 Properties
static uint8 glieseProfileChar8Props = GATT_PROP_READ | GATT_PROP_WRITE;
// Characteristic 8 Value
// The first byte is passcode
static uint8 glieseProfileChar8[GLIESEPROFILE_CHAR8_LEN] = {0};
// Gliese Profile Characteristic 8 User Description
static uint8 glieseProfileChar8UserDesp[7] = "Dione1\0";

// Gliese Profile Characteristic 9 Properties
static uint8 glieseProfileChar9Props = GATT_PROP_READ | GATT_PROP_WRITE;
// Characteristic 9 Value
// The first byte is passcode
static uint8 glieseProfileChar9[GLIESEPROFILE_CHAR9_LEN] = {0};
// Gliese Profile Characteristic 9 User Description
static uint8 glieseProfileChar9UserDesp[7] = "Dione2\0";

// Gliese Profile Characteristic 10 Properties
static uint8 glieseProfileChar10Props = GATT_PROP_READ | GATT_PROP_WRITE;
// Characteristic 10 Value
// The first six bytes is passcode
static uint8 glieseProfileChar10[GLIESEPROFILE_CHAR10_LEN] = {0};
// Gliese Profile Characteristic 10 User Description
static uint8 glieseProfileChar10UserDesp[5] = "Eris\0";
/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t glieseProfileAttrTbl[SERVAPP_NUM_ATTR_SUPPORTED] =
{
    // Gliese Profile Service
    {
        { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
        GATT_PERMIT_READ,                         /* permissions */
        0,                                        /* handle */
        (uint8 *) &glieseProfileService           /* pValue */
    },

    // Characteristic 1 Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &glieseProfileChar1Props
    },

    // Characteristic Value 1
    {
        { ATT_UUID_SIZE, glieseProfilechar1UUID },
        GATT_PERMIT_WRITE | GATT_PERMIT_READ,
        0,
        glieseProfileChar1
    },

    // Characteristic 1 User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        glieseProfileChar1UserDesp
    },

    // Characteristic 2 Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &glieseProfileChar2Props
    },
    // Characteristic Value 2
    {
        { ATT_UUID_SIZE, glieseProfilechar2UUID },
        GATT_PERMIT_WRITE,
        0,
        glieseProfileChar2
    },
    // Characteristic 2 User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        glieseProfileChar2UserDesp
    },

    // Characteristic 3 Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &glieseProfileChar3Props
    },
    // Characteristic Value 3
    {
        { ATT_UUID_SIZE, glieseProfilechar3UUID },
        GATT_PERMIT_WRITE,
        0,
        glieseProfileChar3
    },
    // Characteristic 3 User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        glieseProfileChar3UserDesp
    },

    // Characteristic 4 Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &glieseProfileChar4Props
    },

    // Characteristic Value 4
    {
        { ATT_UUID_SIZE, glieseProfilechar4UUID },
        0,
        0,
        &glieseProfileChar4
    },

    // Characteristic 4 configuration
    {
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        (uint8 *)glieseProfileChar4Config
    },

    // Characteristic 4 User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        glieseProfileChar4UserDesp
    },

    // Characteristic 5 Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &glieseProfileChar5Props
    },

    // Characteristic Value 5
    {
        { ATT_UUID_SIZE, glieseProfilechar5UUID },
        GATT_PERMIT_AUTHEN_READ,
        0,
        glieseProfileChar5
    },

    // Characteristic 5 User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        glieseProfileChar5UserDesp
    },

    // Characteristic 6 Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &glieseProfileChar6Props
    },

    // Characteristic Value 6
    {
        { ATT_UUID_SIZE, glieseProfilechar6UUID },
        GATT_PERMIT_WRITE,
        0,
        glieseProfileChar6
    },

    // Characteristic 6 User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        glieseProfileChar6UserDesp
    },

    // Characteristic 7 Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &glieseProfileChar7Props
    },

    // Characteristic Value 7
    {
        { ATT_UUID_SIZE, glieseProfilechar7UUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        glieseProfileChar7
    },

    // Characteristic 7 User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        glieseProfileChar7UserDesp
    },

    // Characteristic 8 Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &glieseProfileChar8Props
    },

    // Characteristic Value 8
    {
        { ATT_UUID_SIZE, glieseProfilechar8UUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        glieseProfileChar8
    },

    // Characteristic 8 User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        glieseProfileChar8UserDesp
    },

    // Characteristic 9 Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &glieseProfileChar9Props
    },

    // Characteristic Value 9
    {
        { ATT_UUID_SIZE, glieseProfilechar9UUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        glieseProfileChar9
    },

    // Characteristic 9 User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        glieseProfileChar9UserDesp
    },

    // Characteristic 10 Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &glieseProfileChar10Props
    },

    // Characteristic Value 10
    {
        { ATT_UUID_SIZE, glieseProfilechar10UUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        glieseProfileChar10
    },

    // Characteristic 10 User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        glieseProfileChar10UserDesp
    },
};

static int8 gChaZhi = 0;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static uint8 glieseProfile_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                       uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen );
static bStatus_t glieseProfile_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
        uint8 *pValue, uint8 len, uint16 offset );

static void glieseProfile_HandleConnStatusCB( uint16 connHandle, uint8 changeType );


/*********************************************************************
 * PROFILE CALLBACKS
 */
// Gliese Profile Service Callbacks
CONST gattServiceCBs_t glieseProfileCBs =
{
    glieseProfile_ReadAttrCB,  // Read callback function pointer
    glieseProfile_WriteAttrCB, // Write callback function pointer
    NULL                       // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      GlieseProfile_AddService
 *
 * @brief   Initializes the Gliese Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 *
 * @return  Success or Failure
 */
bStatus_t GlieseProfile_AddService( uint32 services )
{
    uint8 status = SUCCESS;

    // Initialize Client Characteristic Configuration attributes
    GATTServApp_InitCharCfg( INVALID_CONNHANDLE, glieseProfileChar4Config );

    // Register with Link DB to receive link status change callback
    VOID linkDB_Register( glieseProfile_HandleConnStatusCB );

    if ( services & GLIESEPROFILE_SERVICE )
    {
        // Register GATT attribute list and CBs with GATT Server App
        status = GATTServApp_RegisterService( glieseProfileAttrTbl,
                                              GATT_NUM_ATTRS( glieseProfileAttrTbl ),
                                              &glieseProfileCBs );
    }

    return ( status );
}


/*********************************************************************
 * @fn      GlieseProfile_RegisterAppCBs
 *
 * @brief   Registers the application callback function. Only call
 *          this function once.
 *
 * @param   callbacks - pointer to application callbacks.
 *
 * @return  SUCCESS or bleAlreadyInRequestedMode
 */
bStatus_t GlieseProfile_RegisterAppCBs( glieseProfileCBs_t *appCallbacks )
{
    if ( appCallbacks )
    {
        glieseProfile_AppCBs = appCallbacks;

        return ( SUCCESS );
    }
    else
    {
        return ( bleAlreadyInRequestedMode );
    }
}


/*********************************************************************
 * @fn      GlieseProfile_SetParameter
 *
 * @brief   Set a Gliese Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to right
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t GlieseProfile_SetParameter( uint8 param, uint8 len, void *value )
{
    bStatus_t ret = SUCCESS;
    switch ( param )
    {
    case GLIESEPROFILE_CHAR1:
        if ( len <= GLIESEPROFILE_CHAR1_LEN )
        {
            VOID osal_memcpy( glieseProfileChar1, value, len );
        }
        else
        {
            ret = bleInvalidRange;
        }
        break;

    case GLIESEPROFILE_CHAR2:
        if ( len <= GLIESEPROFILE_CHAR2_LEN )
        {
            VOID osal_memcpy( glieseProfileChar2, value, len );
        }
        else
        {
            ret = bleInvalidRange;
        }
        break;

    case GLIESEPROFILE_CHAR3:
        if ( len < GAP_DEVICE_NAME_LEN - 2 )
        {
            VOID osal_memcpy( glieseProfileChar3, value, len );
        }
        else
        {
            ret = bleInvalidRange;
        }
        break;

    case GLIESEPROFILE_CHAR4:
        if ( len == sizeof ( uint8 ) )
        {
            glieseProfileChar4 = *((uint8 *)value);

            // See if Notification has been enabled
            GATTServApp_ProcessCharCfg( glieseProfileChar4Config, &glieseProfileChar4, FALSE,
                                        glieseProfileAttrTbl, GATT_NUM_ATTRS( glieseProfileAttrTbl ),
                                        INVALID_TASK_ID );
        }
        else
        {
            ret = bleInvalidRange;
        }
        break;

    case GLIESEPROFILE_CHAR5:
        if ( len == GLIESEPROFILE_CHAR5_LEN )
        {
            VOID osal_memcpy( glieseProfileChar5, value, GLIESEPROFILE_CHAR5_LEN );
        }
        else
        {
            ret = bleInvalidRange;
        }
        break;

    case GLIESEPROFILE_CHAR6:
        if ( len == GLIESEPROFILE_CHAR6_LEN )
        {
            VOID osal_memcpy( glieseProfileChar6, value, GLIESEPROFILE_CHAR6_LEN );
        }
        else
        {
            ret = bleInvalidRange;
        }
        break;

     case GLIESEPROFILE_CHAR7:
        if ( len == GLIESEPROFILE_CHAR7_LEN )
        {
            //uint8 data[GLIESEPROFILE_CHAR7_LEN];
            //VOID osal_memcpy(data, value, GLIESEPROFILE_CHAR7_LEN );
            uint8 *data = (uint8*)value;
            gChaZhi = data[8];
            glieseProfileChar7[8] = gChaZhi;
        }
        else
        {
            ret = bleInvalidRange;
        }
        break;

    case GLIESEPROFILE_CHAR8:
        if ( len == GLIESEPROFILE_CHAR8_LEN )
        {
            VOID osal_memcpy( glieseProfileChar8+1, (uint8*)value + 1, GLIESEPROFILE_CHAR8_LEN-1 );
        }
        else
        {
            ret = bleInvalidRange;
        }
        break;

    case GLIESEPROFILE_CHAR9:
        if ( len == GLIESEPROFILE_CHAR9_LEN )
        {
            VOID osal_memcpy( glieseProfileChar9+1, (uint8*)value + 1, GLIESEPROFILE_CHAR6_LEN-1 );
        }
        else
        {
            ret = bleInvalidRange;
        }
        break;

    case GLIESEPROFILE_CHAR10:
        if ( len == (GLIESEPROFILE_CHAR10_LEN-6) )
        {
            VOID osal_memcpy( glieseProfileChar10+6, (uint8*)value, GLIESEPROFILE_CHAR10_LEN-6 );
        }
        else
        {
            ret = bleInvalidRange;
        }
        break;
    default:
        ret = INVALIDPARAMETER;
        break;
    }

    return ( ret );
}

/*********************************************************************
 * @fn      GlieseProfile_GetParameter
 *
 * @brief   Get a Gliese Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to put.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t GlieseProfile_GetParameter( uint8 param, void *value )
{
    bStatus_t ret = SUCCESS;
    switch ( param )
    {
    case GLIESEPROFILE_CHAR1:
        VOID osal_memcpy( value, glieseProfileChar1, GLIESEPROFILE_CHAR1_LEN);
        break;

    case GLIESEPROFILE_CHAR2:
        VOID osal_memcpy( value, glieseProfileChar2, GLIESEPROFILE_CHAR2_LEN);
        break;

    case GLIESEPROFILE_CHAR3:
        VOID osal_memcpy( value, glieseProfileChar3, GAP_DEVICE_NAME_LEN);
        break;

    case GLIESEPROFILE_CHAR4:
        *((uint8 *)value) = glieseProfileChar4;
        break;

    case GLIESEPROFILE_CHAR5:
        VOID osal_memcpy( value, glieseProfileChar5, GLIESEPROFILE_CHAR5_LEN );
        break;

    case GLIESEPROFILE_CHAR6:
        VOID osal_memcpy( value, glieseProfileChar6, GLIESEPROFILE_CHAR6_LEN );
        break;

    case GLIESEPROFILE_CHAR7:
        VOID osal_memcpy( value, glieseProfileChar7, GLIESEPROFILE_CHAR7_LEN );
        break;

    case GLIESEPROFILE_CHAR10:
        VOID osal_memcpy( value, glieseProfileChar10, GLIESEPROFILE_CHAR10_LEN );
        break;
    default:
        ret = INVALIDPARAMETER;
        break;
    }

    return ( ret );
}

/*********************************************************************
 * @fn          glieseProfile_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 *
 * @return      Success or Failure
 */
static uint8 glieseProfile_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                       uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen )
{
    bStatus_t status = SUCCESS;

    // If attribute permissions require authorization to read, return error
    if ( gattPermitAuthorRead( pAttr->permissions ) )
    {
        // Insufficient authorization
        return ( ATT_ERR_INSUFFICIENT_AUTHOR );
    }

    // Make sure it's not a blob operation (no attributes in the profile are long)
    if ( offset > 0 )
    {
        return ( ATT_ERR_ATTR_NOT_LONG );
    }

    if ( pAttr->type.len == ATT_BT_UUID_SIZE )
    {
#if 0
        // 16-bit UUID
        uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
        switch ( uuid )
        {
            // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
            // gattserverapp handles those reads

            // characteristics 1 and 2 have read permissions
            // characteritisc 3 does not have read permissions; therefore it is not
            //   included here
            // characteristic 4 does not have read permissions, but because it
            //   can be sent as a notification, it is included here
        case GLIESEPROFILE_CHAR1_UUID:
        case GLIESEPROFILE_CHAR2_UUID:
        case GLIESEPROFILE_CHAR4_UUID:
            *pLen = 1;
            pValue[0] = *pAttr->pValue;
            break;

        case GLIESEPROFILE_CHAR5_UUID:
            *pLen = GLIESEPROFILE_CHAR5_LEN;
            VOID osal_memcpy( pValue, pAttr->pValue, GLIESEPROFILE_CHAR5_LEN );
            break;

        default:
            // Should never get here! (characteristics 3 and 4 do not have read permissions)
            *pLen = 0;
            status = ATT_ERR_ATTR_NOT_FOUND;
            break;
        }
#endif
    }
    else
    {
        // 128-bit UUID
        if (osal_memcmp(glieseProfilechar3UUID, pAttr->type.uuid, 16) == TRUE)
        {
            *pLen = GAP_DEVICE_NAME_LEN - 3;
            VOID osal_memcpy( pValue, pAttr->pValue, GAP_DEVICE_NAME_LEN - 3 );
        }
        else if (osal_memcmp(glieseProfilechar1UUID, pAttr->type.uuid, 16) == TRUE)
        {
            *pLen = GLIESEPROFILE_CHAR1_LEN;
            VOID osal_memcpy( pValue, pAttr->pValue, GLIESEPROFILE_CHAR1_LEN );
        }
        else if (osal_memcmp(glieseProfilechar5UUID, pAttr->type.uuid, 16) == TRUE)
        {
            *pLen = GLIESEPROFILE_CHAR5_LEN;
            VOID osal_memcpy( pValue, pAttr->pValue, GLIESEPROFILE_CHAR5_LEN );
        }
        else if (osal_memcmp(glieseProfilechar10UUID, pAttr->type.uuid, 16) == TRUE)
        {
            *pLen = GLIESEPROFILE_CHAR10_LEN;
            VOID osal_memcpy( pValue, pAttr->pValue+6, GLIESEPROFILE_CHAR10_LEN-6 );
        }
        else if (osal_memcmp(glieseProfilechar7UUID, pAttr->type.uuid, 16) == TRUE)
        {
            *pLen = GLIESEPROFILE_CHAR7_LEN;
            uint16 temperature = HalAdcTemperatureInternalRead();
            temperature = (uint16)((int16)temperature + gChaZhi);
            glieseProfileChar7[4] = (temperature >> 8) & 0xFF;
            glieseProfileChar7[5] = temperature & 0xFF; 
            VOID osal_memcpy( pValue, pAttr->pValue, GLIESEPROFILE_CHAR7_LEN );
        }
        else if (osal_memcmp(glieseProfilechar8UUID, pAttr->type.uuid, 16) == TRUE)
        {
            *pLen = GLIESEPROFILE_CHAR8_LEN-1;
            VOID osal_memcpy( pValue, pAttr->pValue+1, GLIESEPROFILE_CHAR8_LEN-1 );
        }
        else if (osal_memcmp(glieseProfilechar9UUID, pAttr->type.uuid, 16) == TRUE)
        {
            *pLen = GLIESEPROFILE_CHAR9_LEN-1;
            VOID osal_memcpy( pValue, pAttr->pValue+1, GLIESEPROFILE_CHAR9_LEN-1 );
        }
        else if (osal_memcmp(glieseProfilechar4UUID, pAttr->type.uuid, 16) == TRUE)
        {
            *pLen = 1;
            pValue[0] = *pAttr->pValue;
        }
        else if (osal_memcmp(glieseProfilechar6UUID, pAttr->type.uuid, 16) == TRUE)
        {
            *pLen = GLIESEPROFILE_CHAR6_LEN;
            VOID osal_memcpy( pValue, pAttr->pValue, GLIESEPROFILE_CHAR6_LEN );
        }
        else
        {
            // Should never get here! (characteristics 3 and 4 do not have read permissions)
            *pLen = 0;
            status = ATT_ERR_ATTR_NOT_FOUND;
        }
    }

    return ( status );
}

/*********************************************************************
 * @fn      glieseProfile_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 *
 * @return  Success or Failure
 */
static bStatus_t glieseProfile_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
        uint8 *pValue, uint8 len, uint16 offset )
{
    bStatus_t status = SUCCESS;
    uint8 notifyApp = 0xFF;

    // If attribute permissions require authorization to write, return error
    if ( gattPermitAuthorWrite( pAttr->permissions ) )
    {
        // Insufficient authorization
        return ( ATT_ERR_INSUFFICIENT_AUTHOR );
    }

    if ( pAttr->type.len == ATT_BT_UUID_SIZE )
    {
        // 16-bit UUID
        uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
        switch ( uuid )
        {
#if 0
        case GLIESEPROFILE_CHAR1_UUID:
        case GLIESEPROFILE_CHAR3_UUID:

            //Validate the value
            // Make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len != 1 )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }

            //Write the value
            if ( status == SUCCESS )
            {
                uint8 *pCurValue = (uint8 *)pAttr->pValue;
                *pCurValue = pValue[0];

                if( pAttr->pValue == &glieseProfileChar1 )
                {
                    notifyApp = GLIESEPROFILE_CHAR1;
                }
                else
                {
                    notifyApp = GLIESEPROFILE_CHAR3;
                }
            }

            break;
#endif
        case GATT_CLIENT_CHAR_CFG_UUID:
            status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                     offset, GATT_CLIENT_CFG_NOTIFY );
            break;

        default:
            // Should never get here! (characteristics 2 and 4 do not have write permissions)
            status = ATT_ERR_ATTR_NOT_FOUND;
            break;
        }
    }
    else
    {
        // 128-bit UUID
        //status = ATT_ERR_INVALID_HANDLE;
        if(osal_memcmp(glieseProfilechar3UUID, pAttr->type.uuid, 16) == TRUE)
        {
            // Validate the value
            // Make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > GAP_DEVICE_NAME_LEN - 2 )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }

            // Write the value
            if ( status == SUCCESS )
            {
                osal_memset(glieseProfileChar3, 0, GAP_DEVICE_NAME_LEN);
                VOID osal_memcpy( glieseProfileChar3, pValue, len );
                notifyApp = GLIESEPROFILE_CHAR3;
            }
        }
        else if (osal_memcmp(glieseProfilechar1UUID, pAttr->type.uuid, 16) == TRUE)
        {
            //Validate the value
            // Make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > GLIESEPROFILE_CHAR1_LEN )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }
            // Write the value
            if ( status == SUCCESS )
            {
                osal_memset(glieseProfileChar1, 0, GLIESEPROFILE_CHAR1_LEN);
                VOID osal_memcpy( glieseProfileChar1, pValue, len );
                notifyApp = GLIESEPROFILE_CHAR1;
            }
        }
        else if (osal_memcmp(glieseProfilechar6UUID, pAttr->type.uuid, 16) == TRUE)
        {
            //Validate the value
            // Make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > GLIESEPROFILE_CHAR6_LEN )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }
            // Write the value
            if ( status == SUCCESS )
            {
                osal_memset(glieseProfileChar6, 0, GLIESEPROFILE_CHAR6_LEN);
                VOID osal_memcpy( glieseProfileChar6, pValue, len );
                notifyApp = GLIESEPROFILE_CHAR6;
            }
        }
        else if (osal_memcmp(glieseProfilechar10UUID, pAttr->type.uuid, 16) == TRUE)
        {
            //Validate the value
            // Make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > GLIESEPROFILE_CHAR10_LEN )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }
            // Write the value
            if ( status == SUCCESS )
            {
                osal_memset(glieseProfileChar10, 0, GLIESEPROFILE_CHAR10_LEN);
                VOID osal_memcpy( glieseProfileChar10, pValue, len );
                notifyApp = GLIESEPROFILE_CHAR10;
            }
        }
        else if (osal_memcmp(glieseProfilechar8UUID, pAttr->type.uuid, 16) == TRUE)
        {
            //Validate the value
            // Make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len != GLIESEPROFILE_CHAR8_LEN )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }
            // Write the value
            if ( status == SUCCESS )
            {
                if (pValue[0] != 0x6D)
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
                else
                {
                    uint8 i;
                    for (i = 1; i < 16; i+=3)
                    {
                        if (pValue[i] != 0) break;
                    }
                    uint16 willSetTemp = pValue[i+1] | (uint16)(pValue[i] << 8);
                    uint16 curTemperature = HalAdcTemperatureInternalRead();
                    int16 chaZhi = willSetTemp - curTemperature;
                    int16 abschaZhi = ABS(chaZhi);
                    if (abschaZhi >= 90)
                    {
                        status = ATT_ERR_ATTR_NOT_FOUND;
                    }
                    else
                    {
                        glieseProfileChar8[i] = (curTemperature >> 8) && 0xFF;
                        glieseProfileChar8[i+1] = curTemperature && 0xFF;
                        glieseProfileChar8[i+2] = chaZhi;
//                        gChaZhi = chaZhi;
                        notifyApp = GLIESEPROFILE_CHAR8;
                    }
                }
            }
        }
        else if (osal_memcmp(glieseProfilechar7UUID, pAttr->type.uuid, 16) == TRUE)
        {
            //Validate the value
            // Make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len != GLIESEPROFILE_CHAR7_LEN )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }
            // Write the value
            if ( status == SUCCESS )
            {
                //osal_memset(glieseProfileChar7, 0, GLIESEPROFILE_CHAR7_LEN);
                VOID osal_memcpy( glieseProfileChar7, pValue, 4 );
                uint16 willSetTemp = pValue[7] | (uint16)(pValue[6] << 8);
                uint16 temperature = HalAdcTemperatureInternalRead();
                int16 chaZhi = willSetTemp - temperature;
                int16 abschaZhi = ABS(chaZhi);
                if (abschaZhi >= 90)
                {
                    status = ATT_ERR_ATTR_NOT_FOUND;
                }
                else
                {
                    glieseProfileChar7[8] = chaZhi;
                    gChaZhi = chaZhi;
                    notifyApp = GLIESEPROFILE_CHAR7;
                }
            }
        }
        else if(osal_memcmp(glieseProfilechar2UUID, pAttr->type.uuid, 16) == TRUE)
        {
            // Validate the value
            // Make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > GLIESEPROFILE_CHAR2_LEN )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }

            // Write the value
            if ( status == SUCCESS )
            {
                osal_memset(glieseProfileChar2, 0, GLIESEPROFILE_CHAR2_LEN);
                VOID osal_memcpy( glieseProfileChar2, pValue, len );
                notifyApp = GLIESEPROFILE_CHAR2;
            }
        }
        else
        {
            // Should never get here! (characteristics 2 and 4 do not have write permissions)
            status = ATT_ERR_ATTR_NOT_FOUND;
        }
    }

    // If a charactersitic value changed then callback function to notify application of change
    if ( (notifyApp != 0xFF ) && glieseProfile_AppCBs && glieseProfile_AppCBs->pfnGlieseProfileChange )
    {
        glieseProfile_AppCBs->pfnGlieseProfileChange( notifyApp );
    }

    return ( status );
}

/*********************************************************************
 * @fn          glieseProfile_HandleConnStatusCB
 *
 * @brief       Gliese Profile link status change handler function.
 *
 * @param       connHandle - connection handle
 * @param       changeType - type of change
 *
 * @return      none
 */
static void glieseProfile_HandleConnStatusCB( uint16 connHandle, uint8 changeType )
{
    // Make sure this is not loopback connection
    if ( connHandle != LOOPBACK_CONNHANDLE )
    {
        // Reset Client Char Config if connection has dropped
        if ( ( changeType == LINKDB_STATUS_UPDATE_REMOVED )      ||
                ( ( changeType == LINKDB_STATUS_UPDATE_STATEFLAGS ) &&
                  ( !linkDB_Up( connHandle ) ) ) )
        {
            GATTServApp_InitCharCfg( connHandle, glieseProfileChar4Config );
        }
    }
}

//int16 glieseReadTemperatureInternal()
//{
    // Configure ADC and perform a read
    //HalAdcSetReference( HAL_ADC_REF_125V );
    //uint16 adc = HalAdcRead( HAL_ADC_CHN_TEMP, HAL_ADC_RESOLUTION_12 );
//}

/*********************************************************************
*********************************************************************/
